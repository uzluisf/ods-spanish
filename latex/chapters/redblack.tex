\chapter{Árboles Rojos-Negros}
\chaplabel{redblack}

\index{árbol!binario de búsqueda!rojo-negro}%
\index{árbol!rojo-negro}%

En este capítulo, presentamos los árboles rojos-negros, una versión de los
árboles binarios de búsqueda con altura logarítmica. Los árboles rojos-negros
son una de las estructuras de datos más utilizadas. Las mismas aparecen como la
estructura de búsqueda primaria en muchas implementaciones de librerías,
incluyendo Java Collections Framework y varias implementaciones de
C++ Standard Template Library. Igualmente son usadas dentro del núcleo del
sistema operativo Linux. Existen varias razones para la popularidad de los
árboles rojos-negros:

\begin{enumerate}
\item Un árbol rojo-negro que almacena #n# valores tiene una altura máxima de $2\log #n#$.
\item Las operaciones #add(x)# y #remove(x)# sobre un árbol rojo-negro se
    ejecuta en tiempo de \emph{peor caso} $O(\log #n#)$.
\item El número amortizado de rotaciones realizados durante una operación
   #add(x)# o #remove(x)# es constante.
\end{enumerate}

Las dos primeras de estas propiedades ya colocan los árboles rojos-negros
delante de las listas por salto, los treaps y los árboles \textit{scapegoat}.
Las estructuras de listas por salto y los treaps depende en la aleatorización y
sus tiempos de ejecución $O(\log #n#)$ son solo anticipados. Los árboles
\textit{scapegoat} tiene un límite garantizado en su altura, pero
#add(x)# y #remove(x)# solo se ejecutan en tiempo amortizado $O(\log
#n#)$.  La tercera propiedad es solo la guinda del pastel. La misma nos dice
que el tiempo necesario para añadir o remover un elemento #x# es eclipsado por
el tiempo que se toma en encontrar a #x#.\footnote{Nota que las listas por salto
y los treaps también tienen esta propiedad en el sentido anticipado.
Ver los Ejercicios~\ref{exc:skiplist-changes} y \ref{exc:treap-rotates}.}

No obstante, estas propiedades tan sutiles de los árboles rojos-negros vienen
con un precio: la complexidad de la implementación. Mantener un límite de
$2\log #n#$ sobre la altura no es fácil. Esto requiere un análisis cuidadoso de
un número de casos. Debemos asegurar que la implementación haga la cosa correcta
en cada caso. Un rotación o cambio de color fuera de lugar produce un \emph{bug}
que podría ser difícil de entender y encontrar.

En lugar de comenzar inmediatamente con la implementación de los árboles
rojos-negros, primero proveeremos información contextual sobre una estructura de
datos relacionada: los árboles 2-4. Esto nos dará una perspectiva sobre cómo los
árboles negros-rojos fueron descubiertos y por qué el mantenimiento eficiente de
los mismos es posible.

\section{Árboles 2-4}
\seclabel{twofour}

Un árbol 2-4 es un árbol arraigado con las propiedades siguientes:
\begin{prp}[height]
    Todas las hojas tienen la misma profundidad.
\end{prp}
\begin{prp}[degree]
    Cada nodo interno tiene 2, 3, o 4 hijos.
\end{prp}

Un ejemplo de un árbol 2-4 se muestra en la \figref{twofour-example}.
\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/24rb-2}
  \end{center}
  \caption{Un árbol 2-4 de altura 3.}
  \figlabel{twofour-example}
\end{figure}

Las propiedades de los árboles 2-4 implica que su altura es logarítmica en el
número de hojas:
\begin{lem}\lemlabel{twofour-height}
  Un árbol 2-4 con #n# hojas tiene una altura máxima $\log #n#$.
\end{lem}

\begin{proof}
    El límite inferior de 2 sobre el número de hijos de un nodo interno implica
    que, si la altura de un árbol 2-4 es $h$, entonces tiene por lo menos
    $2^h$ hojas.  En otras palabras,
    \[
        #n# \ge 2^h \enspace .
    \]
    Al tomar los logaritmos en ambos lados de esta desigualdad,
    obtenemos $h \le \log #n#$.
\end{proof}

\subsection{Agregando una Hoja}

Agregar una hoja a un árbol 2-4 es fácil  (ver la \figref{twofour-add}).  Si
queremos agregar una hoja #u# como el hijo de algún nodo #w# en el penúltimo
nivel, entonces simplemente convertimos a #u# en un hijo de #w#.  Esto
ciertamente mantiene la propieda de la altura, pero podría violar la propiedad
del grado; si #w# tenía cuatro hijos anterior a la adición de #u#, entonces
#w# tiene ahora cinco hijos.
En este caso, \emph{separamos} a
\index{separación}%
#w# en dos nodos, #w# y #w#', los cuales tienen dos y tres hijos,
respectivamente. Pero ahora #w#' no tiene ningún padre, así que
recusirvamente convertimos a #w#' en un hijo del padre de #w#. Nuevamente, esto
puede causar que el padre de #w# tenga demasiado hijos y por ende, lo separamos.
Este proceso continúa hasta que encontremos un nodo con menos de cuatro
hijos, o hasta que separemos la raíz, #r#, en dos nodos, #r# y #r'#.  En el
último caso, creamos una raíz nueva que tiene a #r# y #r'# como hijos.
Esto incrementa simultáneamente la profundidad de todas las hojas y así mantiene
la propiedad de la altura.

\begin{figure}
  \begin{center}
   \begin{tabular}{c}
     \includegraphics[scale=0.90909]{figs/24tree-add-1} \\
     \includegraphics[scale=0.90909]{figs/24tree-add-2} \\
     \includegraphics[scale=0.90909]{figs/24tree-add-3}
   \end{tabular}
  \end{center}
  \caption[Agregando una hoja a un árbol 2-4]{Agregando una hoja a un árbol 2-4.
  Este proceso finaliza después de una separación debido a que #w.parent# tiene
    un grado menor que 4 posterior a la adición.}
  \figlabel{twofour-add}
\end{figure}

Dado que la altura de un árbol 2-4 nunca excede $\log #n#$, el proceso de añadir
una hoja finaliza después de realizar como máximo $\log #n#$ pasos.

\subsection{Remover una Hoja}

Remover una hoja de un árbol 2-4 es un poco más complicado (ver la
\figref{twofour-remove}). Para remover una hoja #u# de su padre #w#,
solamente la removemos. Si #w# tenía solamente dos hijos previo a la remoción de
#u#, entonces #w# queda con solamente un hijo y viola la propiedad del grado.

\begin{figure}
  \begin{center}
   \begin{tabular}{c}
     \includegraphics[height=\FifthHeightScaleIfNeeded]{figs/24tree-remove-1} \\
     \includegraphics[height=\FifthHeightScaleIfNeeded]{figs/24tree-remove-2} \\
     \includegraphics[height=\FifthHeightScaleIfNeeded]{figs/24tree-remove-3} \\
     \includegraphics[height=\FifthHeightScaleIfNeeded]{figs/24tree-remove-4} \\
     \includegraphics[height=\FifthHeightScaleIfNeeded]{figs/24tree-remove-5} \\
   \end{tabular}
  \end{center}
  \caption[Removiendo una hoja de un árbol 2-4]{Removiendo una hoja de un árbol
    2-4.  Este proceso llega hasta la raíz porque cada uno de los ancestros
    de #u# y sus hermanos tienen solamente dos hijos.}
  \figlabel{twofour-remove}
\end{figure}

Para corregir esto, buscamos por el hermano de #w#, #w'#. La existencia del
nodo #w'# cierta dado que el padre de #w# tenía por los menos dos hijos.
Si #w'# tiene tres o cuatro hijos, entonces tomamos uno de estos hijos de
#w'# y se los damos a #w#. Ahora #w# tiene dos hijos y #w'# tiene dos o tres
hijos y hemos terminado.

Por el otro lado, si #w'# tiene solamente dos hijos, entonces \emph{combinamos}
\index{combinar}%
a #w# y #w'# en un solo nodo, #w#, que tiene tres hijos. Lo siguiente es remover
a #w'# recursivamente del padre de #w'#.  Este proceso termina cuando
alcanzamos un nodo, #u#, donde #u# o su hermano tiene más de dos hijos,
o cuando alcanzamos la raíz.  En el último caso, si la raíz se queda con un solo
hijo, entonces borramos la raíz y convertimos su hijo la nueva raíz.
Nuevamente, esto simultáneamente disminuye la altura de cada hoja y por lo tanto
mantiene la propiedad de la altura.

Otra vez, dado que la altura del árbol nunca excede $\log #n#$,
el proceso de la remoción de una hoja finaliza después de realizar como máximo
$\log #n#$ pasos.


\section{#RedBlackTree#: Un Árbol 2-4 Simulado}
\seclabel{redblacktree}

Un árbol rojo-negro es un árbol binario de búsqueda en el cual cada nodo, #u#,
tiene un \emph{color}
\index{color}%
el cual es \emph{rojo} ó \emph{negro}.  Rojo se representa con el valor
$0$ y negro con el valor $1$.
\index{nodo!rojo}%
\index{nodo!negro}%
\javaimport{ods/RedBlackTree.red.black.Node<T>}
\cppimport{ods/RedBlackTree.RedBlackNode.red.black}

Antes y después de cualquier operación sobre un árbol rojo-negro, las
dos propiedades siguientes son satisfecha. Cada propiedad está definida en
términos de los colores rojo y negro, y en términos de los valores numéricos
0 y 1.
\begin{prp}[black-height]
  \index{propiedad!black-height}%
    Existen el mismo número de nodos negros en cada ruta de la raíz a la hoja.
    (La suma de los colores en cualquier ruta de la raíz a la hoja es la misma.)
\end{prp}

\begin{prp}[no-red-edge]
  \index{propiedad!no-red-edge}%
  No dos nodos rojos son adyacentes. (Por cada nodo #u#, excepto la raíz,
  $#u.colour# + #u.parent.colour# \ge 1$.)
\end{prp}
Nota que siempre podemos colorear la raíz negra, #r#, de un árbol rojo-negro
sin violar ninguna de estas dos propiedades, así que asumiremos que la raíz
es negra, y  los algoritmos para actualizar un árbol rojo-negro mantendrán esto.
Otro truco que simplifica los árboles rojos-negros es trat los nodos externos
(representado por #nil#) como nodos negros. De esta manera, cada nodo real, #u#,
de un árbol rojo-negro tiene exactamente dos hijos, cada uno con un color bien
definido. Un ejemplo de un árbol rojo-negro se muestra en la
\figref{redblack-example}.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/24rb-1}
  \end{center}
  \caption[Un árbol rojo-negro]{Un ejemplo de un árbol rojo-negro con
    altura-negra 3.  Los nodos externos (#nil#) están dibujados como cuadrados.
   }
  \figlabel{redblack-example}
\end{figure}

\subsection{Los Árboles Rojos-Negros y Los Árboles 2-4}

A primera instancia podría ser sorprendente que un árbol rojo-negro pueda
ser actualizado eficientemente para mantener las propiedades de altura-negra y
no-arista, y parece inusual hasta considerar estas propiedades como útiles.
Sin embargo, los árboles rojos-negros fueron diseñados para ser una simulación
eficiente de los árboles 2-4 como árboles binarios.

Referirse a la \figref{twofour-redblack}.

Considera cualquier árbol rojo-negro, $T$, que posee #n# nodos y realiza la
transformación siguiente: Remueve cada nodo rojo #u# y conecta los dos hijos
de #u# directamente al padre (negro) de #u#.  Después de esta transformación
nos quedamos con un árbol $T'$ que tiene solamente nodos negros.
\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[scale=0.90909]{figs/24rb-3} \\
      \includegraphics[scale=0.90909]{figs/24rb-2}
    \end{tabular}
  \end{center}
  \caption{Every red-black tree has a corresponding 2-4 tree.}
  \figlabel{twofour-redblack}
\end{figure}

Cada nodo interno en $T'$ tiene dos, tres, o cuatro hijos:
Un nodo negro que comenzó con dos hijos negros tendrán aún dos
hijos negros después de la transformación. Un nodo negro que comenzó
con un hijo rojo y un hijo negro tendrá tres hijos después de esta
transformación. Un nodo negro que comenzó con dos hijos rojos tendrán
cuatro hijos después de esta transformación. En adición, la propiedad
negro-altura ahora garantiza que cada ruta desde la raíz a la hoja en
$T'$ tiene la misma longitud. En otras palabras, $T'$ es un árbol 2-4!

El árbol 2-4 $T'$ tiene $#n#+1$ hojas que corresponden a los $#n#+1$
nodos externos del árbol rojo-negro. Por lo tanto, este árbol tiene
altura máxima $\log (#n#+1)$. Ahora, cada ruta de la raíz a la hoja en
el árbol 2-4 corresponde a una ruta desde la ruta del árbol
rojo-negro $T$ a un nodo externo. El primer y último nodo en esta ruta
son negros y a lo sumo uno de cada dos nodos externos es rojo, así que
esta ruta tiene como máximo $\log(#n#+1)$ nodos negros y como máximo
$\log(#n#+1)-1$ nodos rojos. Por consiguiente, la ruta más larga desde la
raíz a cualquier nodo \emph{interno} en $T$ es a lo sumo
\[
   2\log(#n#+1) -2 \le 2\log #n# \enspace ,
\]
para cualquier $#n#\ge 1$.  Esto demuestra que la propiedad más importante
de los árboles rojos-negros:
\begin{lem}
La altura de un árbol rojo-negro con #n# nodos es como máxima $2\log #n#$.
\end{lem}

Ahora que hemos visto la relación entre los árboles 2-4 y los árboles rojos-negros,
no es difícil creer que podemos mantener eficientemente un árbol rojo-negro
mientras añadimos y removimos elementos.

Ya hemos visto que añadir un elemento a una estructura #BinarySearchTree#
puede hacerse al añadir una hoja nueva. Por lo tanto, para implementar #add(x)#
en un árbol rojo-negro necesitamos un método para simular la división de un
nodo con cinco hijos en un árbol 2-4. Un nodo de un árbol 2-4 con cinco hijos es
representado por un nodo negro que tiene dos hijos rojos, uno de los cuales también
tiene un hijo rojo. Podemos ``dividir'' este nodo al colorearlo rojo y colorear
sus dos hijos negro. Un ejemplo de esto se muestra en la \figref{rb-split}.

\begin{figure}
  \begin{center}
   \begin{tabular}{c}
     \includegraphics[scale=0.90909]{figs/rb-split-1} \\
     \includegraphics[scale=0.90909]{figs/rb-split-2} \\
     \includegraphics[scale=0.90909]{figs/rb-split-3} \\
   \end{tabular}
  \end{center}
  \caption[Simulando un árbol 2-4]{Simulando una operación de división de un
    árbol 2-4 durante un adición al mismo árbol. (Esto simula la adición al
    árbol 2-4 que se muestra en la \figref{twofour-add}.)}
  \figlabel{rb-split}
\end{figure}

Similarmente, la implementación de #remove(x)# requiere un método para fusionar
dos nodos y tomar prestado un hijo de un nodo hermano. La fusión de dos nodos
es el inverso de una división (mostrado en la \figref{rb-split}), e involucra la coloración
roja de dos hermanos negros y la coloración negra de su padre rojo.
Tomar prestado de un nodo hermano es los más complicado de los procedimientos
e involucra las rotaciones y coloraciones de los nodos.

Por supuesto, durante todo esto debemos mantener la propiedad \textit{no arista roja}
y la propiedad \textit{negra-altura}. Cuando esto no sorprendente que esto pueda hacerse,
existen un gran número de casos que tienen que ser considerados si tratamos
de hacer una simulación directa de un árbol 2-4 por árbol rojo-negro.
En algún punto, se vuelve más simple ignorar el árbol 2-4 subyacente y trabajar
directamente hacia el mantenimiento de las propiedades del árbol rojo-negro.

\subsection{Árboles Rojos-Negros de Izquierda}
\index{árbol!rojo-negro}%
\index{árbol!rojo-negro de izquierda}%

En general, no existe una definición definitiva de los árboles rojos-negros.
En lugar de esto, existe una familia de estructuras que gestiona y mantiene
las propiedades \textit{no arista roja} y \textit{negra-altura}
durante las operaciones #add(x)# y #remove(x)#. Diferentes estructuras hacen esto en
maneras diferentes. Aquí, implementamos una estructura de datos que llamamos
#RedBlackTree#.
\index{estructuras de datos!RedBlackTree@#RedBlackTree#}%
Esta estructura implementa una variante particular de los árboles rojos-negros
que satisface una propiedad adicional:
\begin{prp}[left-leaning]\prplabel{left-leaning}\prplabel{redblack-last}
  \index{propiedad!left-leaning}%
  En cualquier nodo #u#, si #u.left# es negro, entonces #u.right# es negro.
\end{prp}
Nota que el árbol rojo-negro mostrado en la \figref{redblack-example} no satisface
la propiedad \textit{de izquierda}; la misma es violada por el padre del nodo
rojo en la ruta del extremo derecho.

La razón para mantener la propiedad de izquierda es que reduce el número de
casos encontrados cuando se actualiza el árbol durante las operaciones
#add(x)# y #remove(x)#. En términos de árboles 2-4, esto implica que
un árbol 2-4 tiene una representación única: Un nodo de grado dos se convierte
un nodo negro con dos hijos negros. Un nodo de grado tres se convierte un
nodo negro cuyo hijo izquierdo es rojo y cuyo hijo derecho es negro. Un
nodo de grado cuatro se convierte en un nodo negro con dos hijos rojos.

Antes que describamos la implementación de #add(x)# y #remove(x)# en detalle,
primero presentamos algunas subrutinas simples usada por estos métodos que
se ilustran en la \figref{redblack-flippullpush}.  Las primeras dos subrutinas
son para manipular los colores mientras se preserva la propiedad negro-altura.
El método #pushBlack(u)# toma como entrada un nodo negro #u# que tiene dos
hijos rojos y colorea a #u# rojo y sus hijos negros. El método
#pullBlack(u)# hace lo inverso de esta operación:
%\codeimport{ods/RedBlackTree.pushBlack(u).pullBlack(u)}

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/flippullpush}
  \end{center}
  \caption{Flips, pulls and pushes}
  \figlabel{redblack-flippullpush}
\end{figure}

El método #flipLeft(u)# intercambia los colores de #u# y #u.right#
y después realiza una rotación izquierda en #u#.  Este método invierte
los colores de estos dos nodos y también su relación de padre-hijo:
%\codeimport{ods/RedBlackTree.flipLeft(u)}

La operación #flipLeft(u)# es especialmente útil para restaurar la propiedad
de izquierda en un nodo #u# que la viola (porque #u.left# es negro y #u.right#
es rojo red). En este caso especial, podemos estar confiados que esta operación
preserva las propiedades \textit{no arista roja} y \textit{negra-altura}.
La operación #flipRight(u)# es simétrica con #flipLeft(u)#, cuando los roles
de la izquierda y de la derecha invertidos.
%\codeimport{ods/RedBlackTree.flipRight(u)}

\subsection{Adición}

Para implementar a #add(x)# en una estructura #RedBlackTree#, realizamos una
inserción estándar de #BinarySearchTree# para agregar una hoja nueva, #u#, con
$#u.x#=#x#$ y asignar $#u.colour#=#red#$.  Nota que esto no cambia la altura
negra de ningún nodo, así que no viola la propiedad altura-negra. Podría, sin
embargo, violar la propiedad left-leaning (si #u# es el hijo derecho de su
padre), y podría violar la propiedad no-red-edge (si el padre de #u# es #rojo#).
Para restaura estas propiedades, llamamos el método #addFixup(u)#.
%\codeimport{ods/RedBlackTree.add(x)}

Ilustrado en la \figref{rb-addfix}, el método #addFixup(u)# toma como entrada
un nodo #u# cuyo color es rojo y que puede violar la propiedad 
no-red-edge y/o la propiedad left-leaning.  La discusión siguiente es
probablemente imposible de seguir sin hacer referencia a la \figref{rb-addfix} 
o al recrearla en un pedazo de paper. De hecho, se le aconseja al lector
que estudia esta figura antes de continuar.

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/rb-addfix}
  \end{center}
  \caption{A single round in the process of fixing Property~2 after
  an insertion.}
  \figlabel{rb-addfix}
\end{figure}

Si #u# es la raíz del árbol, entonces podemos colorear a #u# de negro para
restaurar ambas propiedades. Si los hermanos de #u# son también rojos, entonce
el padre de #u# debe ser negro, así que ambas propiedades de left-leaning and
no-red-edge properties son satisfechas.

Por lo contrario, podemos primero determinar si el padre de #u#, #w#, viola la
propiedad left-leaning, y si lo hace, realizar una operación #flipLeft(w)# y
asignar $#u#=#w#$.  Esto nos deja estado bien definido:  #u# es el hijo
izquierdo de su padre, #w#, así que #w# ahora satisface la propiedad
left-leaning. Todo lo que resta es asegurarnos de la propiedad no-red-edge en
#u#.  Solo tenemos que preocuparnos sobre el caso en el cual #w# es rojo, dado
que #u# ya satisface la propiedad no-red-edge.

Debido que no hemos terminado aún, #u# es rojo y #w# es rojo. La propiedad
no-red-edge (la cual es únicamente violada por #u# y no por #w#) implica que 
el abuelo #g# de #u#  existe y es negro. Si el hijo derecho de #g# es rojo,
entonces la propiedad left-leaning asegura que ambos hijos de #g# son rojos,
y una llamada a #pushBlack(g)# colorea a #g# de rojo y a #w# de negro. Esto
restaura la propiedad no-red-edge en #u#, pero puede causar que sea violada 
#g#, así que el proceso entero comienza con $#u#=#g#$.

Si el hijo derecho de #g# es negro, entonces una llamada a #flipRight(g)# 
hace a #w# el padre (negro) de #g# y da a #w# dos hijos rojos, #u# y
#g#. Esto asegura que #u# satisface la propiedad no-red-edge y #g#
satisface la propiedad left-leaning.  En este caso podemos terminar. 
%\codeimport{ods/RedBlackTree.addFixup(u)}

El método #insertFixup(u)# toma tiempo constante por iteración y cada iteración
finaliza o acerca a #u# a la raíz. Por lo tanto, el método
#insertFixup(u)# finaliza después $O(\log #n#)$ iteraciones en tiempo
$O(\log #n#)$.

\subsection{Remoción}

% HERE
La operación #remove(x)# en una estructura #RedBlackTree# es la más complicada
de implementar, y esto verdadero para todas las variantes conocidas de un 
árbol rojo-negro. Similar a la operación #remove(x)# en una estructura 
\texttt{BinarySearchTree}, esta operación se reduce a encontrar un nodo #w#
con un solo hijo #u, y separar a #w# del árbol al hacer que #w.parent# adopte a
#u#.

El problema con esto es que, si #w# es negro, entonces la propiedad black-height
será ahora violada en #w.parent#.  Podríamos evitar este problema, 
temporariamente, al añadir #w.colour# a #u.colour#. Por supuesto, esto
introduce otros dos problemas: (1)~si #u# y #w# ambos comenzaron negros,
entonces $#u.colour#+#w.colour#=2$ (doblemente negro), el cual es un color
inválido. Si #w# era rojo, entonces es reemplazado por un nodo negro #u#, el
cual viola la propiedad left-leaning en $#u.parent#$.  Ambos problemas pueden
resolverse con una llamada al método  #removeFixup(u)#.
%\codeimport{ods/RedBlackTree.remove(x)

El método #removeFixup(u)# toma como entrada un nodo #u# cuyo color es negro (1) 
o doblemente negro (2).  Si #u# es doblemente negro, entonces #removeFixup(u)#
realiza una serie de rotaciones y operaciones de recoloración que mueve el 
nodo doblemente negro hacia en el árbol hastat que puede ser eliminado.
Durante este proceso, el nodo #u# cambia hasta que, al final de este proceso,
#u# se refiere a la raíz del subárbol que ha sido cambiado. La  raíz de este
subárbol puede haber cambiado de color. En particular, podría haber cambiado de
rojo a negro, así que el método #removeFixup(u)# finaliza al verificar
si el padre de #u# viola la propiedad left-leaning y, si lo hace, arreglarlo.
%\codeimport{ods/RedBlackTree.removeFixup(u)}

El método #removeFixup(u)# se ilustra en la \figref{rb-removefix}.
Nuevamente, el texto siguiente será difícil, si no imposible, de seguir
sin hacer referencia a la \figref{rb-removefix}.  Cada iteración del bucle 
en #removeFixup(u)# procesa el nodo #u# doblemente negro, basado
en uno de los cuatros casos:

\begin{figure}
  \begin{center}
    \includegraphics[height=\HeightScaleIfNeeded]{figs/rb-removefix}
  \end{center}
  \caption{Una sola ronda en el proceso de eliminación de un nodo doblemente
    negro después de una remoción.}
  \figlabel{rb-removefix}
\end{figure}

% HERE
\noindent
Caso 0: #u# es la raíz.  Esto es el caso más fácil de tratar. Recoloreamos
a #u# de negro (esto no viola ninguna de las propiedades del árbol rojo-negro).

\noindent
Caso 1: El hermano de #u#, #v#, es rojo.  En este caso, el hermano de #u# es el
hijo izquierdo de su padre, #w# (por la propiedad left-leaning).  Realizamos 
un right-flip en #w# y luego procedemos a la siguiente iteración.  Nota que esta
acción causa que el padre de #w# viole la propiedad left-leaning y
que la profundidad de #u# incremente. Sin embargo, esto también implica que la
siguiente iteración estará en el Caso~3 con #w# de color rojo. Al examinar al
Caso~3 más abajo, veremos que el proceso terminará durante la siguiente
iteración. 
%\codeimport{ods/RedBlackTree.removeFixupCase1(u)}

\noindent
Caso 2: El hermano de #u#, #v#, es negro, y #u# es el hijo izquierdo de su 
padre, #w#.  En este caso, llamamos a #pullBlack(w)#, coloreando a #u# negro,
a  #v# rojo, y oscureciendo el color de #w# a negro o doblemente negro.
En este punto,  #w# no satisface la propiedad left-leaning, así que  llamamos
a #flipLeft(w)# para resolver esto.

En este punto, #w# es rojo y #v# es la raíz del subárbol con el cual comenzamos.
Necesitamos chequear si #w# causa que la propiedad no-red-edge sea violada.
Hacemos esto al inspeccionar el hijo derecho  de #w#, #q#.  Si #q# es negro,
entonces  #w# satisface la propiedad no-red-edge y podemos continuar 
la siguiente iteración con $#u#=#v#$.

De lo contrario (#q# es rojo), así que ambas propiedades de no-red-edge y left-leaning
son violadas en #q# y #w#, respectivamente.  La propiedad left-leaning
es  restaurada con una llamada a #rotateLeft(w)#, pero la propiedad no-red-edge
es aún violada. En este punto,  #q# es el hijo izquierdo de #v#, #w# es el hijo
izquierdo de #q#, #q# y #w# son ambos rojos, y #v#
es negro or doblemente negro.  Una llamada a #flipRight(v)#  hace a #q# el
padre de  #v# y #w#.  Acompañando esto con una llamada a #pushBlack(q)# colorea
a #v# y #w# negros y asigna el color de #q# devuelta al color original de #w#.

En este punto, el nodo doblemente negro ha sido eliminado y las propiedades 
no-red-edge y black-height son reestablecidas. Solamente un posible problema
permanece: el hijo derecho de #v# puede ser rojo, en cual caso
la propiedad left-leaning sería violada.  Chequeamos esto y 
realizamos una llamada a #flipLeft(v)# para corregirlo si necesario.
%\codeimport{ods/RedBlackTree.removeFixupCase2(u)}

\noindent
Caso 3: El hermano de #u# es negor y #u# es el hijo derecho de su padre,
#w#.  Este caso es simétrico al Caso~2 y se maneja casi de la misma manera.
La única diferencia viene del hecho que la propiedad left-leaning
es asimétrica, y por lo tanto requiere un manejo distinto.

Como antes, comenzamos con una llamada a #pullBlack(w)#, la cual colorea a  #v#
de rojo y a #u# de negro.  Una llamada  a #flipRight(w)# promueve a #v# a la
raíz del subárbol. En este punto #w# es rojo, y el código se bifurca dependiendo
en el color del hijo izquierdo de #w#, #q#.

Si #q# es rojo, entonces el código finaliza exactamente de la misma manera
que el Caso~2, pero es aún mucho más simple dado que no existe ningún peligro 
de que #v# no satisfaga la propiedad left-leaning.

El caso más complicado ocurre cuando #q# es negro. En este caso,
examinamos el color del hijo izquierdo de #v. Si es rojo, entonces #v# tiene
dos hijos rojos y su extra negro puede empujarse hacia abajo con una llamada
a #pushBlack(v)#.  En este punto, #v# ahora tiene el color original de #w#, 
y entonces hemos terminado.

Si el hermano izquierdo de #v# es negro, entonces #v# viola la propiedad
left-leaning, y restauramos esto con una llamada a #flipLeft(v)#. Podemos
entonces retornar el nodo #v# así que la siguiente iteración de
#removeFixup(u)# entonces continúa con $#u#=#v#$.

%\codeimport{ods/RedBlackTree.removeFixupCase3(u)}.

Cada iteración de #removeFixup(u)# toma tiempo constante. Casos~2 y 3
finalizan o acercan a #u# a la raíz del árbol.  Caso~0 (donde
#u# es la raíz) siempre termina y Caso~1 conduce inmediatamente a Caso~3,
el cual también termina. Dado que la altura del árbol es como máxima $2\log
#n#$, concluimos que existen como máximo $O(\log #n#)$ iteraciones de
#removeFixup(u)#, así que #removeFixup(u)# se ejecuta en tiempo $O(\log #n#)$.

\section{Resumen}
\seclabel{redblack-summary}

El teorema siguiente sintetiza el rendimiento de la estructura de datos
#RedBlackTree#:

\begin{thm}
    Una estructura #RedBlackTree# implementa la interfaz #SSet# y
    soporta las operaciones #add(x)#, #remove(x)#, y #find(x)# en tiempo
    $O(\log#n#)$ de peorcaso por cada operación.
\end{thm}

No incluido en el teorema anterior se encuentra el bono extra siguiente:

\begin{thm}\thmlabel{redblack-amortized}
    Al comenzar con una estructura #RedBlackTree# vacía, cualquier secuencia
    de $m$ operaciones #add(x)# y #remove(x)# resulta en un total de
    de tiempo $O(m)$ utilizado durante todas las llamadas de
    #addFixup(u)# y #removeFixup(u)#.
\end{thm}

Aquí presentamos solamente un bosquejo de la demostración de
\thmref{redblack-amortized}. Al comparar #addFixup(u)# y #removeFixup(u)#
con los algoritmos para añadir o remover una hoja en árbol 2-4,
nos podemos convencer que esta propiedad se hereda de un árbol 2-4.
En particular, si podemos mostrar que el tiempo total usado al dividir,
fusionar y tomar prestado de un árbol 2-4 es $O(m)$, entonces esto
implica el \thmref{redblack-amortized}.

La demostración de este teorema para árboles 2-4 usa el método potencial
\index{método!potencial}%
de análisis amortizado.
\footnote{Ver las demostraciones de
\lemref{dualarraydeque-amortized} y \lemref{selist-amortized} para
otras aplicaciones del método potencial.}
Define el potencial de un nodo interno #u# en un árbol 2-4 como
\[
  \Phi(#u#) =
    \begin{cases}
      1 & \text{si #u# tiene 2 hijos} \\
      0 & \text{si #u# tiene 3 hijos} \\
      3 & \text{si #u# tiene 4 hijos}
    \end{cases}
\]
y el potencial de un árbol 2-4 como la suma de los potenciales de sus nodos.
Cuando una división ocurre, es porque un nodo con cuatro hijos se convierte
en dos nodos, con dos y tres hijos. Esto significa que el potencial general
disminuye por $3-1-0 = 2$. Cuando una fusión ocurre, dos nodos que tenían
dos hijos son reemplazados por un nodo con tres hijos. El resultado es una
descenso en un potencial de $2-0=2$.  Por lo tanto, por cada división
o fusión, el potencial disminuye por dos.

Siguiente nota que, si ignoramos la división y fusión de nodos, existe
solamente un número constante de noso cuyos número de hijos cambia con la
adición o remoción de una hoja, Al añadir un nodo, uno de los nodos
tiene su número de hijos incrementado por uno, lo cual incrementa el potencial
por tres como máximo. Durante la remoción de una hoja, un nodo tiene su número
de hijos disminuido por uno, lo cual incrementa el potencial por uno como
máximo, y dos nodos puede estar involucrados en un operación de
\textit{préstamo}, la cual incrementa el potencial total por uno como
máximo.

Para sintetizar, cada fusión y división causa que el potencial se reduzca
por los menos dos. Ignorando fusión y división, cada adición o remoción
causa que el potencial incrementa como máximo tres, y el potencial es siempre
no-negativo. Por consiguiente, el número de divisiones y fusiones causado por
$m$ adiciones o remociones en un árbol inicialmente vacío es como máximo
$3m/2$. \thmref{redblack-amortized} es una consecuencia de este análisis
y la correspondencia entre árboles 2-4 y árboles rojos-negros.
\section{Discusión y Ejercicios}

Los árboles rojos-negros fueron introducidos por primera vez por
Guibas and Sedgewick \cite{gs78}. A pesar de su alta complejidad de su
implementación, este tipo de árboles se encuentran en algunas de las librerías
y aplicaciones más usadas. La mayoría de los libros de texto sobre
algoritmos y estructuras de datos discuten alguna variante de los aŕboles
rojos-negros.

Andersson \cite{a93} describe una versión left-leaning de los árboles
balanceados que es similar a los árboles rojos-negros pero que tiene la
restricción adicional que cualquier nodo tiene como máximo un hijo rojo.
Esto implica que estos árboles simulan árboles 2-3 en lugar de árboles 2-4.
Los mismos son significativamente más simples que la estructura
#RedBlackTree# presentanda en este capítulo.

Sedgewick \cite{s08} describe dos versiones de los árboles negros-rojos
left-leaning. Estos usan recursión con una simulación de separación de arriba
hacia abajo y mezcla en árboles 2-4. La combinación de estas dos técnicas crean
código particularmente breve y elegante.

Una estructura de datos relaciondad, y más antigua, es el
\emph{árbol AVL} \cite{avl62}.
\index{árbol!AVL}%
Los árboles AVL son \emph{balanceados por altura}:
\index{balanceado!por altura}%
\index{árbol!binario de búsqueda!balanceado por altura}%

En cada nodo $u$, la altura del subárbol arraigado en #u.left# and el subárbol
arraigado en #u.right# difieren como máximo por uno. Sigue inmediatamente que 
, si $F(h)$ es el número mínimo de hojas en un árbol de altura $h$, entonces
$F(h)$ obedece la recurrencia Fibonacci
\[
   F(h) = F(h-1) + F(h-2)
\]
con los casos bases $F(0)=1$ y $F(1)=1$.  Esto significa que  $F(h)$ es
aproximadamente $\varphi^h/\sqrt{5}$, donde
$\varphi=(1+\sqrt{5})/2\approx1.61803399$ es el 
\emph{número aúreo}.  (Más precisament, $|\varphi^h/\sqrt{5} - F(h)|\le 1/2$.)
Argumentando como en la demostración de \lemref{twofour-height}, esto implica 
que 
\[
   h \le \log_\varphi #n# \approx 1.440420088\log #n# \enspace ,
\]
así que los árboles AVL tiene una altura mucho menor que los árboles
rojos-negros. La altura balanceada puede mantenerse durante las operaciones
#add(x)# y #remove(x)# al caminar devuelta la ruta desde la raíz y realizar una
operación de rebalanceo en cada nodo #u# donde la altura de los árboles
izquierdos y derechos de #u# difieren por dos. Ver \figref{avl-rebalance}.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/avl-rebalance}
  \end{center}
  \caption{Rebalanceo en un árbol AVL.  Como máximo dos rotatciones son
    requeridas para convertir un nodo cuya estructura tiene una altura de 
    $h$ y $h+2$ a un nodo cuyo subárboles tienen cada uno altura máxima $h+1$.}
  \figlabel{avl-rebalance}
\end{figure}

La variante de Sedgewick de los árboles rojos-negros, y los árboles AVL
son todos más simple de implementar que la estructura #RedBlackTree#
definida aquí. Desafortunadamente, ninguno de ellos garantizan que el
tiempo amortizado utilizado en el rebalanceo se
$O(1)$ por actualización. En particular, no existe un análogo del
\thmref{redblack-amortized} para esas estructuras.

\begin{figure}
  \centering{\includegraphics[scale=0.90909]{figs/redblack-example}}
  \caption{A red-black tree on which to practice.}
  \figlabel{redblack-example2}
\end{figure}

\begin{exc}
  Ilustra un árbol 2-4 que corresponde a la estructura #RedBlackTree# en la
  \figref{redblack-example2}.
\end{exc}

\begin{exc}
    Ilustra la adición de 13, después 3.5, después 3.3 sobre la estructura
    #RedBlackTree# en la \figref{redblack-example2}.
\end{exc}

\begin{exc}
    Ilustra la remocción de 11, después 9, después 5 sobre la estructura
    #RedBlackTree# en la \figref{redblack-example2}.
\end{exc}

\begin{exc}
    Muestra que, para valores de #n# arbitrariamente grandes, existen árboles
    rojos-negros que tienen una altura $2\log #n#-O(1)$.
\end{exc}

\begin{exc}
    Considera las operaciones #pushBlack(u)# y #pullBlack(u)#. ¿Qué hacen estas
    operaciones al árbol 2-4 subyacente que está siendo simulado por el árbol
    rojo-negro?
\end{exc}

\begin{exc}
    Muestra que, para valores de #n# arbitrariamente grandes, existen secuencias
    de las operaciones #add(x)# y #remove(x)# que conllevan a árboles
    rojos-negroscon #n# nodos que tienen una altura $2\log #n#-O(1)$.
\end{exc}

\begin{exc}
    ¿Por qué el método #remove(x)# en la implementación de #RedBlackTree#
    realizan la asignación #u.parent=w.parent#? ¿No se hizo esto con la
    llamada a #splice(w)#?
\end{exc}

\begin{exc}
    Supón un árbol 2-4, $T$, que tiene $#n#_\ell$ hojas y $#n#_i$ nodos
    internos.
  \begin{enumerate}
    \item ¿Cuál es el valor mínimo de $#n#_i$, como una función de $#n#_\ell$?
    \item ¿Cuál es el valor máximo de $#n#_i$, como una función de $#n#_\ell$?
    \item Si $T'$ es un árbol rojo-negro que representa $T$, entonces cuántos
        nodos rojos tiene $T'$?
  \end{enumerate}
\end{exc}

\begin{exc}
    Supón que se te da un árbol binario de búsqueda con #n# nodos y una altura
    $2\log #n#-2$ como máxima.  ¿Es siempre posible colorear los nodos rojos y
    negros  de tal manera que el árbol satisfaga las propiedades de la
    altura-negra y no-arista-roja? Si lo es, puede hecerse a satisfacer la
    propiedad left-leaning?
\end{exc}

\begin{exc}\exclabel{redblack-merge}
    Supón que tienes dos árboles rojos-negros $T_1$ y $T_2$ que poseen la misma
    altura-negra, $h$, y de tal manera que la llave más grande en
    $T_1$ es menor que la llava más pequeñas en $T_2$.  Muestra cómo combinar
    a $T_1$ y $T_2$ en un árbol rojo-negro único en tiempo $O(h)$.
\end{exc}

\begin{exc}
    Extiende tu solución al \excref{redblack-merge} al caso donde los dos árboles
    $T_1$ y $T_2$ tienen alturas negras diferentes, $h_1\neq h_2$.
    El tiempo de ejecución debería ser $O(\max\{h_1,h_2\})$.
\end{exc}

\begin{exc}
    Demuestra que, durante una operación #add(x)#, un árbol AVL debe realizar
    como máximo una operación de rebalanceo (que involucra como máximo dos
    rotaciones; ver la \figref{avl-rebalance}).  Dado un ejemplo de un árbol
    AVL y una operación #remove(x)# en dicho árbol que requiere
    $\log #n#$ operaciones de rebalanceo.
\end{exc}

\begin{exc}
    Implementa una clase #AVLTree# que implementa los árboles AVL como se
    describen más arriba. Compara su rendimiento a la implementación de la
    estructura #RedBlackTree#. ¿Cuál implementación tiene la operación
    #find(x)# más rápida?
\end{exc}

\begin{exc}
    Diseña e implementa una serie de experimentos que compara el rendimiento
    relativo de #find(x)#, #add(x)#, y #remove(x)# para las implementaciones
    #SSet# de las estructuras #SkiplistSSet#, #ScapegoatTree#, #Treap#,
    y #RedBlackTree#.  Asegúrate de incluir los escenarios de pruebas múltiples,
    incluyendo los casos donde los datos son aleatorios, están ordenados,
    se remueven en orden aleatorio, se remueven ordernadamente, y así
    sucesivamente.
\end{exc}
