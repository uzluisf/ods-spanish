\chapter{Estructuras de Datos para Enteros}

En este capítulo, retornamos al problema sobre la  implementación de una
estructura #SSet#. La diferencia ahora es que asumimos los elementos almacenados
en la estructura #SSet# son enteros de #w#-bit. Es decir, queremos implementar
#add(x)#, #remove(x)#, y #find(x)# donde $#x#\in\{0,\ldots,2^{#w#}-1\}$. No es
difícil pensar sobre las multitid de aplicaciones donde los datos---o por lo
menos la llave que usamos para ordenar los datos---es un entero.

Discutiremos tres estructuras de datos, cada una basada en las ideas de la
idea previa. La primera estructura, #BinaryTrie#, realiza las tres operaciones
de una estructura #SSet# en tiempo $O(#w#)$. Esto no es muy impresionante,
dado que cualquier subconjunto de $\{0,\ldots,2^{#w#}-1\}$ tiene un tamaño
$#n#\le 2^{#w#}$, así que $\log #n# \le #w#$.  Todas las otras implementaciones
de #SSet# en este libro realizan todas las operaciones en tiempo $O(\log #n#)$
y por lo tanto todas son tan rápidas como #BinaryTrie#.

La segunda estructura, #XFastTrie#, acelera la búsqueda en una estructura
#BinaryTrie# al usar hashing.  Con esta rapidez, la operación #find(x)#
se ejecuta en tiempo $O(\log #w#)$. Sin embargo, las operaciones #add(x)# y
#remove(x)# en una #XFastTrie# aún toma tiempo $O(#w#)$ y el espacio usado
por una #XFastTrie# es $O(#n#\cdot#w#)$.

La tercera estructura de datos, #YFastTrie#, usa una estructura #XFastTrie#
para almacenar solamente una muestra de aproximadamente uno de cada
$#w#$ elementos y almacenar los elementos restantes en una estructura
#SSet# estándar.  Esto truco reduce el tiempo de ejecución de
#add(x)# y #remove(x)# a $O(\log #w#)$ y disminuye el espacio a $O(#n#)$.

Las implementaciones usadas como ejemplos  en este capítulo pueden almacenar
cualquier tipo de datos, siempre y cuando un entero puede asociarse con el
mismo. En las muestras de código, la variable #ix# es siempre el valor entero
asociado con #x#, y el método \javaonly{#in.#}#intValue(x)# convierte a #x# en
su entero asociado. En el texto, sin embargo, trataremos a #x# simplemente
como si fuera un entero.

\section{#BinaryTrie#: Un Árbol Digital de Búsqueda}
\seclabel{binarytrie}

\index{estructuras de datos!BinaryTrie@#BinaryTrie#}%
Una estructura de datos #BinaryTrie# codifica un conjunto de enteros de #w#
bit en un árbol binario. Todas las hojas en el árbol tiene una profundidad
#w# y cada entero está codificado como una ruta raíz-a-hoja. La ruta para el
entero #x# va hacia la izquierda al nivel #i# si el bit #i#th más significante
de #x# es un 0 y va hacia la derecha si es un 1.
\figref{binarytrie-ex} muestra un ejemplo para el caso $#w#=4$,
en el cual el trie almacena los enteros 3(0011), 9(1001), 12(1100),
y 13(1101).
\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/binarytrie-ex-1}
  \end{center}
  \caption{Los enteros almacenados en un trie binario son codificados como rutas
    raíz-a-hoja.}
  \figlabel{binarytrie-ex}
\end{figure}

\index{ruta!búsqueda en una #BinaryTrie#}%
Debido a que la ruta de búsqueda para un valor #x# depende en los bits de #x#,
será útil nombrar los hijos de un nodo, #u#, #u.child[0]# (#izquierdo#)
y #u.child[1]# (#derecho#). Estos punteros hijos actualmente servirán un
deber doble. Dado que las hojas en un trie binario no tienen hijos, los punteros
son usados para concatenar las hojas en una lista doblemente enlazada.
Para una hoja en el trie binario #u.child[0]# (#prev#) es el nodo que
viene antes que #u# en la lista y #u.child[1]# (#next#) es el nodo que
sigue a #u# en la lista. Un nodo especial, #dummy#, es  usado antes que el
primer nodo y después que el último nodo en la lista (ver \secref{dllist}).
\cpponly{En las muestras de códigos, #u.child[0]#, #u.left#, y #u.prev# se refieren
al mismo atributo en el nodo #u#, como lo hacen #u.child[1]#, #u.right#, y #u.next#.}

Cada nodo, #u#, también contiene un puntero adicional #u.jump#. Si el hijo
izquierdo de #u# está ausente, entonces #u.jump# apunta a la hoja más pequeña
en el sub-árbol de #u#. Si el hijo derecho de #u# está ausente, entonces #u.jump#
apunta a la hoja más grande en el sub-árbol de #u#.
Un ejemplo de una estructura #BinaryTrie#, mostrando los punteros #jump# y la
lista doblemente enlazada en las hojas, se muestra en \figref{binarytrie-ex2}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/binarytrie-ex-2}
  \end{center}
  \caption[Una estructura BinaryTrie]{Una #BinaryTrie# con punteros #jump#
  ilustrados como aristas curvas de trazos.}
  \figlabel{binarytrie-ex2}
\end{figure}

%\jxavaimport{ods/BinaryTrie.Node<Node}
%\cxppimport{ods/BinaryTrie.BinaryTrieNode<Node}

La operación #find(x)# en una estructura #BinaryTrie# es relativamente simple.
Tratamos de seguir la ruta de búsqueda para #x# en el trie. Si alcanzamos una hoja,
entonces hemos encontrando a #x#. Si alcanzamos un nodo #x# donde no podemos
proceder (debido a que #u# no tiene un hijo), entonces seguimos #u.jump#, lo cual
toma la hoja más pequeña que es más mayor que #x# o la hoja más grande que es más
pequeña #x#. Cuál de estos dos casos ocurre depende en si a #u# le falta su hijo
izquierdo o su hijo derecho, respectivamente.  En el primer caso (a #u# le falta
su hijo izquierdo), hemos encontrado el nodo que queremos. En el último caso (a #u#
le falta su hijo derecho), podemos usar la lista enlazada para alcanzar el nodo
que queremos. Cada uno de estos casos se ilustra en \figref{binarytrie-find}.
\codeimport{ods/BinaryTrie.find(x)}
\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/binarytrie-ex-3}
  \end{center}
  \caption[Rutas de búsqueda en una estructura BinaryTrie]{Las rutas seguidas
  por #find(5)# y #find(8)#.}
  \figlabel{binarytrie-find}
\end{figure}
El tiempo de ejecución del método #find(x)# es determinado por el tiempo
que toma en seguir la ruta desde la raíz a la hoja, así que se ejecuta en tiempo
$O(#w#)$.

La operación #add(x)# en una estructura #BinaryTrie# es igualmente sencilla,
pero tiene que hacer mucho trabajo:
\begin{enumerate}
  \item Sigue la ruta de búsqueda para #x# hasta alcanzar un nodo #u#
    donde no puede proceder.
  \item Crea el resto de la ruta de búsqueda desde #u# a una hoja que contiene
    a  #x#.
  \item Agrega el nodo, #u'#, que contiene a #x# a la lista enlazada
    de hojas (tiene el acceso al predecesor, #pred#, de #u'#
    en la lista enlazada desde el puntero #jump# del último nodo, #u#,
    encontrando durante el paso~1.)
  \item Camina devuelta en la ruta de búsqueda para #x# ajustando
  los punteros #jump# a los nodos cuyo puntero #jump# debería ahora apuntar
  a #x#.
\end{enumerate}

Una adición se ilustra en \figref{binarytrie-add}.
\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/binarytrie-add}
  \end{center}
  \caption[Añadiendo a una estructura BinaryTrie]{Agregando los valores 2 y 15 a
      la estructura #BinaryTrie# en
  \figref{binarytrie-ex2}.}
  \figlabel{binarytrie-add}
\end{figure}
\codeimport{ods/BinaryTrie.add(x)}
Este método realiza una caminata hacia abajo en la ruta de búsqueda para #x# y
una caminata devuelta. Cada paso de estas caminatas toma tiempo constante, así
que el método #add(x)# se ejecuta en tiempo $O(#w#)$.

La operación #remove(x)# deshace el trabajo de la operación #add(x)#. Igual que
#add(x)#, tiene mucho trabajo que hacer:
\begin{enumerate}
  \item It follows the search path for #x# until reaching the leaf, #u#,
  containing #x#.
  \item It removes #u# from the doubly-linked list.
  \item It deletes #u# and then walks back up the search path for #x#
  deleting nodes until reaching a node #v# that has a child that is not
  on the search path for #x#.
  \item It walks upwards from #v# to the root updating any #jump# pointers
  that point to #u#.
\end{enumerate}

Una remoción se ilustra en \figref{binarytrie-remove}.
\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/binarytrie-remove}
  \end{center}
  \caption[Removiendo de una estructura BinaryTrie]{Removiendo el valore 9 de
      la estructura #BinaryTrie# en
  \figref{binarytrie-ex2}.}
  \figlabel{binarytrie-remove}
\end{figure}
\codeimport{ods/BinaryTrie.remove(x)}

\begin{thm}
Una estructura #BinaryTrie# implementa la interfaz #SSet# para enteros de
#w#-bit. #BinaryTrie# supporta las operaciones #add(x)#, #remove(x)#, y
#find(x)# en tiempo $O(#w#)$ por operación. El espacio usado por una estructura
#BinaryTrie#  que almacena #n# valores es $O(#n#\cdot#w#)$.
\end{thm}

\section{#XFastTrie#: Buscando en Tiempo Logarítmico Doble}
\seclabel{xfast}

\index{estructuras de datos!XFastTrie@#XFastTrie#}%
El rendimiento de la estructura #BinaryTrie# no es muy impresionante.
El número de elementos, #n#, almacenado en la estructura es como máximo $2^{#w#}$,
así que $\log #n#\le #w#$. En otras palabras, cualquiera de las estructuras #SSet#
basadas en comparaciones que fueron descritas en otras partes de este libro son
por lo menos tan eficientes como una estructura #BinaryTrie#, y no están restrictas
a almacenar solamente números enteros.

Siguiente describimos la estructura #XFastTrie#, la cual es solamente una estructura
#BinaryTrie# con #w+1# tablas hash---una por cada nivel del trie. Estas tablas hash
son usadas para acelerar la operación #find(x)# a tiempo $O(\log #w#)$. Recuerda
que la operación #find(x)# en una estructura #BinaryTrie# está casi completo una vez que
alcanzamos un nodo, #u#, donde la ruta de búsqueda para #x# le gustaría proceder hacia
#u.right# (o #u.left#) pero #u# no posee un hijo derecho (o izquierdo respectivamente).
En este punto, la búsqueda usa a #u.jump# para saltar a una hoja, #v#, de la estructura
#BinaryTrie# y retorna a #v# o a su sucesor en la lista enlazada de hojas. Una estructura
#XFastTrie# acelera el proceso de búsqueda al usar búsqueda binaria
\index{búsqueda!binaria}%
en los niveles del trie para localizar el nodo #u#.

Para usar búsqueda binaria, necesitamos una manera de determinar si el nodo #u#
que estamos buscando se encuentra encima de un nivel en particular, #i#, de #u#
or debajo del nivel #i#.  Esta información es dada por los #i# bits de orden superior
en la representación binaria de #x#; estos bits determinan la ruta de búsqueda
que #x# toma desde la raíz al nivel #i#.  Para un ejemplo,
refiérete a \figref{xfast-path}; en esta figura el último nodo, #u#,
en la ruta de búsqueda para 14 (cuya representación binaria es 1110) es el nodo
con la etiqueta $11{\star\star}$ en el nivel 2 porque no hay nodo etiquetado 
$111{\star}$ en el nivel 3.  Así que, podemos etiquetar cada nodo en el nivel #i#
con un entero de #i#-bit.  Entonces, el nodo #u# que estamos buscando estaría
en el nivel #i# or debajo del mismo si y solo si hay un nodo en el nivel #i#
cuya etiqueta coincide con los #i# bits de orden superior de #x#.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/xfast-path}
  \end{center}
  \caption{Debido a que no hay un nodo etiquetado $111\star$, la ruta de búsqueda
    para 14 (1110) termina con el nodo etiquetado $11{\star\star}$ .}
  \figlabel{xfast-path}
\end{figure}

En una estructura #XFastTrie#, almacenamos, por cada $#i#\in\{0,\ldots,#w#\}$, todos
los nodos en el nivel #i# en una estructura #USet#, #t[i]#, que es implementada como
una tabla hash (\chapref{hashing}).  El uso de #USet# nos permite verificar
en tiempo anticipado constante si hay un nodo en el nivel #i# cuya etiqueta
coincide con los #i# bits de orden superior de #x#.  De hecho, podemos encontrar
este nodo usando
\javaonly{#t[i].find(x>>>(w-i))#}%
\cpponly{#t[i].find(x>>(w-i))#}%
\pcodeonly{#t[i].find(x>>(w-i))#}%

Las tablas hash $#t[0]#,\ldots,#t[w]#$ nos permite usar búsqueda binaria
para encontrar a #u#. Inicialmente, sabemos que #u# se encuentra en algún nivel
#i# con $0\le #i#< #w#+1$. Por lo tanto inicializamos $#l#=0$ y $#h#=#w#+1$
y repetidamente examinamos la tabla hash #t[i]#, donde $#i#=\lfloor
(#l+h#)/2\rfloor$. Si $#t[i]#$ contiene un nodo cuya etiqueta coincide
los #i# bits de orden superior de #x#, entonces asignamos #l=i# (#u# se encuentra
a o debajo del nivel #i#); de lo contrario, asignamos #h=i# (#u# es
encima del nivel #i#).  Este proceso termina cuando $#h-l#\le 1$, en cual caso
determinamos que #u# es al nivel #l#.  Después completamos la operación #find(x)#
usando #u.jump# y la lista doblemente enlazada de hojas.
\codeimport{ods/XFastTrie.find(x)}
Cada iteración del bucle #while# en el método anterior disminuye a #h-l#
por aproximadamente un factor de dos, así que este bucle encuentra #u# después
de $O(\log #w#)$ iteraciones. Cada iteración realiza un monto constante de trabajo
y un operación #find(x)# en una estructura #USet#, lo cual toma un monto constante
de tiempo anticipado. El trabajo restante toma solamente tiempo constante, 
así que el método #find(x)# en una estructura #XFastTrie# toma solamente tiempo
anticipado $O(\log#w#)$.

Los métodos #add(x)# y #remove(x)# para una estructura #XFastTrie# son casi
idéntico a los mismos métodos de una estructura #BinaryTrie#.  Las únicas modificaciones
son para gestionar las tablas hash #t[0]#,\ldots,#t[w]#.  Durante la operación
#add(x)#, cuando un nodo nuevo es creado en el nivel #i#, este nodo
se añade a #t[i]#.  Durante una operación #remove(x)#, cuando un nodo
se remueve del nivel #i#, este nodo es removido de #t[i]#.  Debido
que la adición y remoción en una tabla hash toma tiempo constante anticipado.
esto no incrementa los tiempos de ejecución de #add(x)# y #remove(x)# por más
de un factor constante. Omitimos código para #add(x)# y #remove(x)#
dado que el código es casi idéntico a código extenso proveído para los
mismos métodos en una estructura #BinaryTrie#.

El teorema siguiente sintetiza el rendimiento de una estructura #XFastTrie#:

\begin{thm}
Una estructura #XFastTrie# implementa la interfaz #SSet# pata enteros con #w#-bit. Una
estructura #XFastTrie# soporta las operaciones
\begin{itemize}
\item #add(x)# y #remove(x)# en tiempo anticipado $O(#w#)$ por operación y
\item #find(x)# en tiempo anticipado $O(\log #w#)$ por operación.
\end{itemize}
El espacio usado por una estructura #XFastTrie# que almacena
#n# valores es $O(#n#\cdot#w#)$.
\end{thm}

\section{#YFastTrie#: Una Estructura #SSet# con Tiempo Logarítmico Doble}
\seclabel{yfast}

Las estructura #XFastTrie# es un mejoramiento vasto---y hasta exponencial---sobre la
estructura #BinaryTrie# en término de tiempo de consulta, pero las operaciones
#add(x)# and #remove(x)# no son aún increíblemente rápidas.  Más aún, el uso de espacio,
$O(#n#\cdot#w#)$, es mayor que las otras implementaciones de #SSet#
descritas en este libro, las cuales todas usan espacio $O(#n#)$.  Estos dos problemas
está relacionados; Si #n# operaciones #add(x)#  construyen una estructura de tamaño
$#n#\cdot#w#$, entonces la operación #add(x)# requiere por lo menos tiempo (y espacio)
en el orden de #w# por operación.

\index{estructuras de datos!YFastTrie@#YFastTrie#}%
La estructura #YFastTrie#, discutida más adelante, simultáneamente mejora el espacio
y velocidad de la estructura #XFastTrie#s.  Una estructura #YFastTrie# usa
#XFastTrie#, #xft#, pero almacena $O(#n#/#w#)$ valores en #xft#.  De esta manera,
el espacio total usado por #xft# es solamente $O(#n#)$.  Además, solamente una de
cada #w# operaciones #add(x)# o #remove(x)# en la estructura #YFastTrie# resulta
en una operación #add(x)# o #remove(x)# en #xft#.  Al hacer esto, el costo promedio incurrido
por las llamadas a las operaciones #add(x)# y #remove(x)# de #xft# es solamente
constante.

La pregunta obvia es entonces: Si #xft# solamente almacena #n#/#w# elementos,
hacia dónde los $#n#(1-1/#w#)$ elementos van?  Estos elementos se mueven
a \emph{estructuras secundarias},
\index{estructura!secundaria}%
en este caso una versión extendida de los treaps (\secref{treap}).
Hay aproximadamente #n#/#w# de estas estructuras secundarias así que,
en promedio, cada una de ellas almacena $O(#w#)$ artículos.  Los treaps
soportan operaciones de #SSet# en tiempo logarítmico, así que las operaciones
en estos treaps se ejecutan en tiempo $O(\log #w#)$, como se requiere.

Más concretamente, una estructura #YFastTrie# contiene una estructura #XFastTrie#, #xft#,
que contiene una muestra aleatoria de los datos, donde cada elemento aparece
en la muestra independientemente con probabilidad $1/#w#$.
Por conveniencia, el valor $2^{#w#}-1$, está siempre contenido en #xft#.
Sean $#x#_0<#x#_1<\cdots<#x#_{k-1}$ los elementos almacenados en #xft#.
Asociado con cada elemento, $#x#_i$, hay un treap, $#t#_i$, que almacena
todos los valores en el rango $#x#_{i-1}+1,\ldots,#x#_i$.  Esto se ilustra
en \figref{yfast}.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/yfast}
  \end{center}
  \caption[Una YFastTrie]{Una estructura #YFastTrie# que contiene los valores 0, 1, 3, 4,
  6, 8, 9, 10, 11, y 13.}
  \figlabel{yfast}
\end{figure}

La operación #find(x)# en una estructura #YFastTrie# es relativamente simple.
Buscamos a #x# en #xft# y encontramos algún valor $#x#_i$ asociado con el treap
$#t#_i$.  Posteriormente usamos el método #find(x)# del treap en $#t#_i$ para
contestar la consulta. El método complemento es un \textit{one-liner}:
\codeimport{ods/YFastTrie.find(x)}
La primera operación #find(x)# (sobre #xft#) toma tiempo $O(\log#w#)$.
La segunda operación #find(x)# (sobre un treap) toma tiempo $O(\log r)$, donde
$r$ es el tamaño del treap.  Más adelante en esta sección, demostraremos que
el tamaño anticipado del treap es $O(#w#)$ así que esta operación toma tiempo
$O(\log #w#)$.
\footnote{Esto es una aplicación de la \emph{Desigualdad de Jensen}: Si $\E[r]=#w#$,
entonces $\E[\log r] \le \log w$.
}

Agregar un elemento a una estructura #YFastTrie# es también relativamente simple---la mayoría
de veces. El método #add(x)# invoca a #xft.find(x)# para localizar al treap,
#t#, en el cual #x# debería ser insertado.  Después invoca #t.add(x)# para agregar
#x# a #t#.  En este punto, lanza una moneda parcial que resulta en caras
con probabilidad $1/#w#$ y en escudos con probabilidad $1-1/#w#$.
Si esta moneda resulta en caras, entonces #x# será agregado a #xft#.

Este es el punto donde las cosas se complican un poco. Cuando #x# se añade a #xf, 
el treap #t# necesita ser dividido en dos treaps, #t1# y #t'#.
El treap #t1# contiene todos los valores menores que o iguales a #x#;
#t'# es el treap original, #t#, con los elementos de #t1# removidos.
Una vez que se hace esto, agregamos el par #(x,t1)# a #xft#.
\figref{yfast-add} muestra un ejemplo.
\codeimport{ods/YFastTrie.add(x)}
\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/yfast-add}
  \end{center}
  \caption[Agregando a una YFastTrie]{Agregando los valores 2 y 6 a una estructura #YFastTrie#. 
  El lanzamiento de moneda para 6 resultó en caras, así que 6 se añadió a #xft# y el treap
  contenía $4,5,6,8,9$ fue dividido.}
  \figlabel{yfast-add}
\end{figure}
Agregar #x# a #t# toma tiempo $O(\log #w#)$.  \excref{treap-split} muestra que la
división de #t# en #t1# y #t'# puede también hacerse en tiempo anticipado $O(\log #w#)$.
Agregar el par (#x#,#t1#) a #xft# toma tiempo $O(#w#)$, pero solamente ocurre
con probabilidad $1/#w#$.  Por lo tanto, el tiempo anticipado de ejecución
de la operación #add(x)# es
\[
    O(\log#w#) + \frac{1}{#w#}O(#w#) = O(\log #w#) \enspace .
\]
El método #remove(x)# deshace el trabajo hecho por #add(x)#.
Usamo a #xft# para encontrar la hoja, #u#, en #xft# que contiene la respuesta
a #xft.find(x)#.  Desde #u#, obtenemos el treap, #t#, que contiene a #x#
y removemos a #x# de #t#.  Si #x# estaba también almacenado en #xft# (y #x#
no es igual a $2^{#w#}-1$) entonces removemos a #x# de #xft# y agregamos los elementos
del treap de #x# al treap, #t2#, que está almacenado en el sucesor de #u# en
la lista enlazada. Esto se ilustra en \figref{yfast-remove}.
\codeimport{ods/YFastTrie.remove(x)}
\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/yfast-remove}
  \end{center}
  \caption[Removiendo de una YFastTrie]{Removiendo los valores 1 y 9 de una #YFastTrie# en \figref{yfast-add}.}
  \figlabel{yfast-remove}
\end{figure}
Encontrar el nodo #u# en #xft# toma tiempo anticipado $O(\log#w#)$.
Remover #x# de #t# toma tiempo anticipado $O(\log#w#)$. Nuevamente,
\excref{treap-split} muestra que la fusión de todos los elementos de #t#
en #t2# puede hacerse en tiempo $O(\log#w#)$.  Si es necesario, remover
a #x# de #xft# toma tiempo $O(#w#)$, pero #x# está contenido solamente en #xft# con
una probabilidad $1/#w#$.  Por lo tanto, el tiempo anticipado para remover un elemento
de #YFastTrie# es $O(\log #w#)$.

Previamente en la discusión, nos demoramos en argumentar sobre los tamaños
de los treaps en esta estructura hasta más tarde. Antes de finalizar este
capítulo, demostramos el resultado que necesitamos.

\begin{lem}\lemlabel{yfast-subtreesize}
Sea #x# un número entero almacenado en una estructura #YFastTrie# y sea $#n#_#x#$
el símbolo que denota el número de elementos en el treap, #t#, que contiene #x#.
Entonces $\E[#n#_#x#] \le 2#w#-1$.
\end{lem}

\begin{proof}
Refiérete a \figref{yfast-sample}. Sean
$#x#_1<#x#_2<\cdots<#x#_i=#x#<#x#_{i+1}<\cdots<#x#_#n#$ los elementos
almacenados en la estructura #YFastTrie#. El treap #t# contiene algunos elementos
mayores que o iguales a #x#.  Estos son $#x#_i,#x#_{i+1},\ldots,#x#_{i+j-1}$,
donde $#x#_{i+j-1}$ es el único de estos elementos en el cual 
el lanzamiento parcial de moneda  realizado en el método #add(x)# resultó en caras.
En otras palabras, $\E[j]$ es igual al número anticipado de lanzamientos parciales
de moneda requeridos para obtener las primeras caras.
\footnote{Este análisis ignora el hecho que $j$ nunca excede a $#n#-i+1$. No obstante,
esto disminuye a $\E[j]$, así que el límite superior todavía se mantiene.}
Cada lanzamiento de moneda es independiente y resulta en caras 
con probabilidad $1/#w#$, así que $\E[j]\le#w#$. (Ver \lemref{coin-tosses} para
un análisis de esto para el caso $#w#=2$.)

Similarmente, los elementos de #t# más pequeños que #x# son
$#x#_{i-1},\ldots,#x#_{i-k}$ donde todas los lanzamientos $k$ de la moneda
resultan en escudo y las lanzadas de la moneda para $#x#_{i-k-1}$ resultan
en caras. Por lo tanto $\E[k]\le#w#-1$, dado que este es el mismo experimento
del lanzamiento de una moneda considerado en el párrafo anterior, pero uno en el
cual el último lanzamiento no se cuenta.
En resumen, $#n#_#x#=j+k$, así que
\[  \E[#n#_#x#] = \E[j+k] = \E[j] + \E[k] \le 2#w#-1 \enspace .  \qedhere \]
\end{proof}
\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/yfast-sample}
  \end{center}
  \caption[El tiempo de consulta en una YFastTrie]{El número de elementos
  en el treap #t# que contienen a #x# es determinado con los experimentos
  de lanzamientos de moneda.}
  \figlabel{yfast-sample}
\end{figure}
%Surprisingly, the bound in \lemref{yfast-subtreesize} is tight.  (If this
%isn't surprising to the reader, they can stop reading this paragraph now.)
%This is counterintuitive because #xft# contains any particular element
%with probability $1/#w#$ so it contains about $n/#w#$ elements.  In other
%words, the average number of elements assigned to one treap is #w#.
%\lemref{yfast-subtreesize} says that the expected size of the treap that
%contains #x# is about twice as large as the average.  This seeming
%discrepancy comes from the fact that larger subtrees contain more elements
%and therefore #x# is more likely to be in a larger subtree than a smaller
%one.

\lemref{yfast-subtreesize} fue la última pieza en la demostración del
teorema siguiente, el cual sintetiza el rendimiento de la estructura
#YFastTrie#:

\begin{thm}
Una estructura #YFastTrie# implementa la interfaz #SSet# para enteros de #w#-bit. A
Una #YFastTrie# soporta las operaciones #add(x)#, #remove(x)#, y #find(x)#
en tiempo anticipado $O(\log #w#)$ por operación. El espacio usado por una
estructura #YFastTrie# que almacena #n# valores es $O(#n#+#w#)$.
\end{thm}

El término #w# en el requerimiento de espacio proviene del hecho que #xft#
siempre almacena el valor $2^#w#-1$.  La implementación podría ser modificada (al costo
de añadir algunos casos extras al código) de tal manera que sea innecesario
almacenar este valor. En este caso, el requerimiento de espacio en el teorema
se convierte en $O(#n#)$.

\section{Discusión y Ejercicios}

La primera estructura de datos que proveyó operaciones #add(x)#,
#remove(x)#, y #find(x)# con tiempo $O(\log#w#)$ fue propuesta por
van~Emde~Boas y desde entonces se conoce como el \emph{árbol estratificado}
(or de \emph{van~Emde~Boas})
\index{árbol!van Emde Boas}%
\index{árbol!estratificado}%
\cite{e77}.
La estructura original de van~Emde~Boas tenía un tamaño
$2^{#w#}$, el cual la hacía impráctica para enteros inmensos.

Las estructuras de datos #XFastTrie# y #YFastTrie# fueron descubiertas por
Willard \cite{w83}.  La estructura #XFastTrie# está íntimamente relacionado
a los árboles de van~Emde~Boas;  por ejemplo, las tablas hash en una #XFastTrie#
reemplaza los arrays en un árbol van~Emde~Boas. Es decir, en lugar de almacenar
la tabla hash #t[i]#, un árbol de van~Emde~Boas almacena un array de longitud
$2^{#i#}$.

Otra estructura para almacenar enteros es los árboles fusiones de
Fredman and Willard \cite{fw93}.
\index{árbol!fusión}%
Esta estructura puede almacenar #n# enteros de #w#-bit en un espacio
$O(#n#)$ así que la operación #find(x)# se ejecuta en tiempo $O((\log #n#)/(\log
#w#))$.  Al usar un árbol fusión cuando $\log #w# > \sqrt{\log #n#}$ y
una estructura #YFastTrie# cuando $\log #w# \le \sqrt{\log #n#}$, uno obtiene un
una estructura de datos con espacio $O(#n#)$ que puede implementar la operación
#find(x)# en tiempo $O(\sqrt{\log #n#})$.  Reciente resultados de límite inferior
de P\v{a}tra\c{s}cu y Thorup \cite{pt07} muestran que estos resultados son más o
menos óptimos, por lo menos para estructuras que usan solamente espacio
$O(#n#)$.

\begin{exc}
    Diseña e implementa una versión simplificada de una estructura
    #BinaryTrie# que no no tiene una lista enlazada o punteros de salto, pero
    para la cual la operación #find(x)# aún se ejecuta en tiempo
    $O(#w#)$.
\end{exc}

\begin{exc}
    Diseña e implementa una implementación simplificada de una estructura
    #XFastTrie# que no utiliza un trie binario. En lugar de esto, tu
    implementación debería almacenar todo en un lista doblemente enlazada
    y $#w#+1$ tablas hash.
\end{exc}

\begin{exc}
    Podemos considerar a #BinaryTrie# como una estructura que almacena cadenas
    de bit de longitud #w# de tal manera que cada cadena de bit es representada
    como una ruta de la raíz a una hoja. Extiende esta idea en una
    implementación #SSet# que almacena cadenas de texto de longitud variable
    e implementa #add(s)#, #remove(s)#, y #find(s)# en tiempo proporcional
    a la longitud a la longitud de #s#.

  \noindent Pista: Cada nodo en tu estructura de datos debería almacenar una
  tabla hash que está indexada por los valores de los caracteres.
\end{exc}

\begin{exc}
    Por un entero $#x#\in\{0,\ldots2^{#w#}-1\}$, deja que $d(#x#)$ denote
    la diferencia entre #x# y el valor retornado por #find(x)#
    [si #find(x)# retorna #null#, entonces define a $d(#x#)$ como $2^#w#$].
    Por ejemplo, si #find(23)# returns 43, entonces $d(23)=20$.
  \begin{enumerate}
    \item Diseña e implementa una versión modificada de la operación #find(x)#
      en una #XFastTrie# que se ejecuta en tiempo anticipado $O(1+\log d(#x#))$
      Pista: La tabla hash $t[#w#]$ contiene todos los valores,
      #x#, de tal manera que $d(#x#)=0$, así que eso sería un buen lugar
      para comenzar.
    \item Diseña e implementa una versión modificada de la operación #find(x)#
      en una estructura #XFastTrie# que se ejecuta en tiempo anticipado
      $O(1+\log\log d(#x#))$.
  \end{enumerate}
\end{exc}


