\chapter{Grafos}
\chaplabel{graphs}

%\textbf{Warning to the Reader:} This chapter is still being actively
%developed, meaning that the code has not been thoroughly tested and/or
%the text has not be carefully proofread.

En este capítulo, estudiamos dos representaciones de grafos y los algoritmos
básicos que usan estas representaciones. 

Matemáticamente, un \emph{grafo (dirigido)}
\index{grafo}%
\index{grafo!dirigido}%
es un par $G=(V,E)$ donde
$V$ es un conjunto de \emph{vértices}
\index{vértice}%
\index{grafo!vértice}%
y $E$ es un conjunto de pares ordenados de vértices llamados \emph{aristas}.
\index{arista}%
\index{grafo!arista}%
Una arista  #(i,j)# es \emph{dirigida}
\index{arista!dirigida}%
desde #i# a #j#;  #i# se conoce como el \emph{origen}
\index{origen}%
\index{arista!origen}%
de la arista y #j# se conoce como el \emph{destino}.
\index{origen}
\index{arista!origen}
Una \emph{ruta}%
\index{ruta}
\index{grafo!ruta}
en $G$ es una secuencia de vértices $v_0,\ldots,v_k$ tal que, por cada 
$i\in\{1,\ldots,k\}$, la arista $(v_{i-1},v_{i})$ se encuentra en $E$. Una
ruta $v_0,\ldots,v_k$ es un \emph{ciclo}
\index{ciclo}%
\index{grafo!ciclo}%
si, adicionalmente, la arista $(v_k,v_0)$ se encuentra en $E$. Una ruta (o
ciclo) es \emph{simple}
\index{grafo!ruta/ciclo simple}%
si todos sus vértices son únicos.  Si hay una ruta desde algún vértice
$v_i$ a otro vértice $v_j$, entonces decimos que $v_j$ es \emph{accesible}
\index{vértice!accesible}
desde $v_i$. Un ejemplo de un grafo se muestra en la \figref{graph}.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/graph}
  \end{center}
  \caption{Un grafo con doces vértices.  Vértices son dibujados como círculos
    y las aristas como curvas apuntando desde el origen al destino.}
  \figlabel{graph}
\end{figure}
%
Debido a su habilidad de modelar muchos fenómenos, los grafos tienen un número
enorme de aplicaciones. Existen muchos ejemplos obvios. Las redes de
computadoras pueden modelarse como grafos, con los vértices que corresponden a las
computadoras y las aristas que corresponden a enlaces (dirigidos) de
comunicación entre esas computadoras. Las calles de una ciudad pueden modelarse
como grafos con los vértices representando las intersecciones y las aristas
representando calles que unen intersecciones consecutivas. 

Ejemplos menos evidentes ocurren tan pronto nos damos cuenta que los grafos
pueden modelar cualquier relación de pareja en un conjunto.  Por ejemplo, en un
ambiente universitario podríamos tener un \emph{grafo de conflicto} para un
calendario
\index{grafo!de conflicto}%
en cual los vértices representan los cursos ofrecidos en la universidad y en
los cuales la arista #(i,j)# está presente si y solo si hay por lo menos un
estudiante que está tomando ambas clases #i# y #j#. Así que, una arista indica
que el examen para la clase #i# no puede programarse al mismo tiempo que el
examen para la clase #j#. 

A lo largo de esta sección, usaremos a #n# para denotar el número de vértices 
de $G$ y #m# para denotar el número de aristas de $G$.  Es decir, $#n#=|V|$
y $#m#=|E|$. Además, asumiremos que $V=\{0,\ldots,#n#-1\}$.
Cualquier otro dator que nos gustaría asociar con los elementos de
$V$ pueden almacenarse en un array de tamaño $#n#$.

Algunas operaciones típicas que pueden realizarse sobre un grafos son:
\begin{itemize}
  \item #addEdge(i,j)#: Agregar la arista $(#i#,#j#)$ a $E$.
  \item #removeEdge(i,j)#: Remover la arista $(#i#,#j#)$ de $E$.
  \item #hasEdge(i,j)#: Verificar si la arista $(#i#,#j#)\in E$ 
  \item #outEdges(i)#: Retornar una #List# de todos los enteros $#j#$ tales que
  $(#i#,#j#)\in E$
  \item #inEdges(i)#: Retornar una #List# de todos los enteros $#j#$ tales que
  $(#j#,#i#)\in E$
\end{itemize}

Nota que estas operaciones no son difíciles de implementar eficientemente.
Por ejemplo, las tres primeras operaciones pueden implementarse 
directamente al usar una estructura #USet#, así que pueden implementarse en
tiempo constante usando tablas hash las cuales fueron discutidas en el
\chapref{hashing}. Las dos últimas operaciones pueden implementarse en tiempo
constante al almacenar, por cada vértice, una lista de sus vértices adyacentes.

Sin embargo, aplicaciones diferentes de grafos tienen diferente requerimientos de 
rendimiento para estas operaciones y, idealmente, podemos usar la implementación
más simple que satisfice todos los requerimientos de la aplicación. Por esta
razón, discutimos dos categorías generales de las representaciones de grafos.

\section{#AdjacencyMatrix#: Representando un Grafo con una Matriz}
\seclabel{adjacency-matrix}

\index{matriz!adyacente}%
Una \emph{matriz adyacente} es una manera de representar un grafo con #n#
vértices $G=(V,E)$ con una matriz $#n#\times#n#$, #a#, cuyas entradas 
son valores booleanos. 

\codeimport{ods/AdjacencyMatrix.a.n.AdjacencyMatrix(n0)}

La entrada #a[i][j]# de la matriz es definida como
\[  #a[i][j]#= 
    \begin{cases}
      #true# & \text{si $#(i,j)#\in E$} \\
      #false# & \text{de lo contrario}
    \end{cases}
\]

La matriz adyacent para el grafo en la \figref{graph} se muestra en la 
\figref{graph-adj}.

En esta representación, las operaciones #addEdge(i,j)#, #removeEdge(i,j)#, y
#hasEdge(i,j)# solo involucran configurar o consultar la entrada #a[i][j]# de la
matriz:

\codeimport{ods/AdjacencyMatrix.addEdge(i,j).removeEdge(i,j).hasEdge(i,j)}
Estas operaciones claramente toman tiempo constante por operación.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/graph} \\[3ex]
    \begin{tabular}{c|cccccccccccc}
        &0&1&2&3&4&5&6&7&8&9&10&11 \\\hline
       0&0&1&0&0&1&0&0&0&0&0&0 &0\\
       1&1&0&1&0&0&1&1&0&0&0&0 &0\\
       2&1&0&0&1&0&0&1&0&0&0&0 &0\\
       3&0&0&1&0&0&0&0&1&0&0&0 &0\\
       4&1&0&0&0&0&1&0&0&1&0&0 &0\\
       5&0&1&1&0&1&0&1&0&0&1&0 &0\\
       6&0&0&1&0&0&1&0&1&0&0&1 &0\\
       7&0&0&0&1&0&0&1&0&0&0&0 &1\\
       8&0&0&0&0&1&0&0&0&0&1&0 &0\\
       9&0&0&0&0&0&1&0&0&1&0&1 &0\\
      10&0&0&0&0&0&0&1&0&0&1&0 &1\\
      11&0&0&0&0&0&0&0&1&0&0&1 &0\\
    \end{tabular} 
  \end{center}
  \caption{Un grafo y su matriz adyacente.}
  \figlabel{graph-adj}
\end{figure}

El lugar donde la matriz adyacente se desempeña pobremente es con las
operaciones #outEdges(i)# y #inEdges(i)#.  Para implementar estas, debemos
escanear todas las #n# entradas en la fila o columna correspondiente de #a# y
acumular todos los índices, #j#, donde #a[i][j]#, respectivamente #a[j][i]#, es
verdadera.

\javaimport{ods/AdjacencyMatrix.outEdges(i).inEdges(i)}
\cppimport{ods/AdjacencyMatrix.outEdges(i,edges).inEdges(i,edges)}
Estas operaciones claramente toman tiempo $O(#n#)$ por operación.  

Otra desventaja de la representación con matriz adyacente es que la misma es
grande. Dicha representación almacena una matriz booleana $#n#\times #n#$,
así que que requiere por lo menos $#n#^2$ bits de memoria. La implementación
aquí usa una matriz de valores \javaonly{#boolean#}\cpponly{#bool#} así que 
actualmente usa aproximadamente $#n#^2$ bytes de memoria.  Una implementación
mucho más cuidadosa, la cual compresa los valores #w# booleanos en una palabra
de memoria, podría reducir este uso del espacio a $O(#n#^2/#w#)$ palabras de
memoria.

\begin{thm}
La estructura de datos #AdjacencyMatrix# implementa la interfaz #Graph#.
Una estructura #AdjacencyMatrix# soporta las operaciones
\begin{itemize}
  \item #addEdge(i,j)#, #removeEdge(i,j)#, y #hasEdge(i,j)# en tiempo
  constante por operación; y
  \item #inEdges(i)#, y #outEdges(i)# en tiempo $O(#n#)$ por operación.
\end{itemize}
El espacio usado por una estructura #AdjacencyMatrix# es  $O(#n#^2)$.
\end{thm}

A pesar de de los requerimientos de uso intensivo de memoria y rendimiento
deficiente de las operaciones #inEdges(i)# y #outEdges(i)#, una estructura
#AdjacencyMatrix# pueden aún ser útil para algunas aplicaciones.
En particular, cuando el grafo $G$ es \emph{denso},
i.e., tiene cerca de $#n#^2$ aristas, entonces el uso de memoria de $#n#^2$
puede ser aceptable.

La estructura de datos #AdjacencyMatrix# es también comúnmente usada porque
las operaciones algebraicas en la matriz #a# puede usarse para calcular
eficientemente las propiedades del grafo $G$. Este es el tema para un curso
sobre algoritmos, pero podemos destacar una de estas propiedades aquí: 
Si tratamos las entradas de #a# como enteros (1 para #true# y 0 para #false#)
y multiplicamos #a# por si misma usando la multiplicación de matriz entonces
obtenemos la matriz $#a#^2$.  Recuerda que, 
de acuerdo a la definición de la multiplicación de matriz, 

\[
    #a^2[i][j]# = \sum_{k=0}^{#n#-1} #a[i][k]#\cdot #a[k][j]# \enspace .
\]

Interpretando esta suma en términos del grafo $G$, esta fórmula cuenta el número
de vértices, $#k#$, tal que $G$ contiene ambas aristas #(i,k)# y #(k,j)#.  Es
decir, la misma cuenta el número de rutas desde $#i#$ a $#j#$
(a través de los vértices inmediatos, $#k#$) cuya longitud es dos.
Esta observación es la fundación de un algoritmo que calcula las rutas más
cortas entre todos los pares de vértices en $G$ usando solamente 
$O(\log#n#)$ multiplicaciones de matrices.

\section{#AdjacencyLists#: Un Grafo como una Colección de Listas}
\seclabel{adjacency-list}

\index{lista!adyacente}%
Las representaciones con \emph{listas adyacentes} de grafos toma un enfoque 
alrededor de los vértices. Existen muchas implementaciones posibles de listas
adyacentes. En esta sección, presentamos una implementación simple. 
Al final de la sección, discutimos diferentes posibilidades. En una
representación con listas adyacentes, el grafo $G=(V,E)$ se representa como un 
array, #adj#, de listas.  La lista #adj[i]# contiene una lista de todos los
vértices adyacentes del vértice #i#. Es decir, dicha lista contiene cada vértice 
#j# tal que $#(i,j)#\in E$.
\codeimport{ods/AdjacencyLists.adj.n.AdjacencyLists(n0)}
(Un ejemplo se muestra en la \figref{graph-adjlist}.)  En esta implementación 
particular, representamos cada lista en #adj# como \javaonly{una}\cpponly{una
sub-clase de} estructura #ArrayStack#, porque nos gustaría acceso con tiempo
constante por posición. Otras opciones también son posibles. Específicamente,
podríamos haber implementado #adj# como una estructura #DLList#.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/graph} \\[3ex]
    \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|}\hline
        0&1&2&3&4&5&6 &7 &8&9 &10&11 \\\hline
        1&0&1&2&0&1&5 &6 &4&8 &9 &10 \\
        4&2&3&7&5&2&2 &3 &9&5 &6 &7 \\
         &6&6& &8&6&7 &11& &10&11& \\
         &5& & & &9&10&  & &  &  & \\
         & & & & &4&  &  & &  &  & \\
    \end{tabular} 
  \end{center}
  \caption{Un grafo y sus listas adyacentes}
  \figlabel{graph-adjlist}
\end{figure}

La operación #addEdge(i,j)# solo agrega el valor #j# a la lista #adj[i]#:
\codeimport{ods/AdjacencyLists.addEdge(i,j)}
Esto toma tiempo constante.

La operación #removeEdge(i,j)# busca a través de la lista #adj[i]# hasta que
encuentra a #j# y después lo remueve:
\codeimport{ods/AdjacencyLists.removeEdge(i,j)}
Esto toma tiempo $O(\deg(#i#))$, donde $\deg(#i#)$ (el \emph{grado}
\index{grafo!grado}%
de
$#i#$) cuenta el número de aristas en $E$ que tienen a $#i#$ como su origen.

La operación #hasEdge(i,j)# es similar;  la misma busca a través de la lista 
#adj[i]# hasta que encuentra a #j# (y retorna verdadero), o llega al final de la
lista (y retorna falso):
\codeimport{ods/AdjacencyLists.hasEdge(i,j)}
Esto también toma tiempo $O(\deg(#i#))$.

La operación #outEdges(i)# es muy simple:
\pcodeonly{retorna la lista #adj[i]#}
\javaonly{retorna la lista #adj[i]#}
\cpponly{copia los valores en #adj[i]# a lista de salida}:
\pcodeimport{ods/AdjacencyLists.outEdges(i)}
\javaimport{ods/AdjacencyLists.outEdges(i)}
\cppimport{ods/AdjacencyLists.outEdges(i,edges)}
\javaonly{Esto claramente toma tiempo constante.}
\cpponly{Esto claramente toma tiempo $O(\deg(#i#))$.}

La operación #inEdges(i)# involucra un poco más de trabajo. Escanea cada vértice
$j$ chequeando si la arista #(i,j)# existes y, si es verdad, agrega a #j#
a lista de salida:
\pcodeimport{ods/AdjacencyLists.inEdges(i)}
\javaimport{ods/AdjacencyLists.inEdges(i)}
\cppimport{ods/AdjacencyLists.inEdges(i,edges)}
Este operación es muy lenta. La misma escanea la lista adyacente de cada
vértice, así que toma tiempo $O(#n# + #m#)$.

El teorema siguiente sintetiza el rendimiento de la estructura de datos de más
arriba:

\begin{thm}
La estructura de datos #AdjacencyLists# implementa la interfaz #Graph#.
Una estructura #AdjacencyLists# soporta las operaciones
\begin{itemize}
  \item #addEdge(i,j)# en tiempo constante por operación;
  \item #removeEdge(i,j)# y #hasEdge(i,j)# en tiempo $O(\deg(#i#))$ por operación;
  \javaonly{\item #outEdges(i)# en tiempo constante por operación; y}
  \cpponly{\item #outEdges(i)# en tiempo $O(\deg(#i#))$ por operación; y}
  \item #inEdges(i)# en tiempo $O(#n#+#m#)$ por operación.
\end{itemize}
El espacio usado por una estructura #AdjacencyLists# es $O(#n#+#m#)$.
\end{thm}

Como se mencionó anteriormente, existen diferentes elecciones que pueden hacerse
cuando se implementa un grafo como una lista adyacente. Algunas preguntas
que surgen son:

\begin{itemize}
  \item ¿Qué tipo de colección debería usarse para almacenar cada elemento 
   #adj#?  Uno podría usar una lista basada en array, una lista enlazada, o hasta
   una tabla hash.
  \item ¿Debería haber una segunda lista adyacente, #inadj#, que almacena,
  por cada #i#, la lista de los vértices, #j#, tal que $#(j,i)#\in E$?
  Esto puede grandemente reducir el tiempo de ejecución de la operación #inEdges(i)#,
  pero requiere un poco más trabajo que añadir o remover aristas. 
  \item ¿Debería la entrada para la arista #(i,j)# en #adj[i]# ser enlazada por
  referencia a la entrada correspondiente en #inadj[j]#?
  \item ¿Deberían aristas ser objectos de primera clase con su propios asociados datos?
  De esta manera, #adj# contendría listas de aristas en lugar de listas de vértices
  (enteros).
\end{itemize}

Muchas de estas preguntas se reducen a un compromiso entre complejidad (y
espacio) de la implementación y el rendimiento de la implementación.

\section{Recorrido de un Grafo}

En esta sección presentamos dos algoritmos para explorar un grafo, 
empezando con uno de sus vértices, #i#, y encontrando todos los vértices
que son accesibles desde #i#. Ambos de estos algoritmos son más adecuados
con grafos que son representados usando la representación con listas adyacentes.
Por lo tanto, al analizar estos algoritmos asumiremos que la representación
subyacente es una estructura #AdjacencyLists#.

\subsection{Búsqueda en Anchura (Breadth-First Search)}

\index{búsqueda!en anchura}%
El algoritmo de \emph{búsqueda en anchura} comienza con el vértice #i# y visite,
primero los vecinos de #i#, después de los vecinos de los vecinos de #i#,
después los vecinos de los vecinos de los vecinos de #i#, y así sucesivamente.

Este algoritmos es una generalización del algoritmo de recorrido de anchura de los
árboles binarios (\secref{bintree:traversal}), y es muy similar; el mismo usa
una cola, #q#, que inicialmente contiene solo a #i#. Después repetidamente
extrae un elemento de  #q# y agrega sus vecinos a #q#, provisto que estos vecinos
nunca han estado en #q# anteriormente. La única gran diferencia entre el 
algoritmo de búsqueda en anchura para los grafos y aquel para los árboles es que
aquel para los grafos tiene que asegurarse que el mismo vértice no se añadido
a #q# más de una vez. El algoritmo hace esto al usar un array auxiliar de valores
booleanos, #seen#, que mantiene un récord de los vértices que se han ya
descubierto.

\codeimport{ods/Algorithms.bfs(g,r)}

Un ejemplo de la ejecución de #bfs(g,0)# sobre el grafo de la \figref{graph}
se muestra en la \figref{graph-bfs}.  Diferente ejecuciones son posibles,
dependiendo en el orden de las listas adyacentes; \figref{graph-bfs}
usa las listas adyacentes en la \figref{graph-adjlist}.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/graph-bfs}
  \end{center}
  \caption[Búsqueda en anchura]{Un ejemplo de búsqueda en anchura comenzando con el nodo 0.
  Los nodos son etiquetados con el orden en el cual son añadidos a #q#. Las aristas
  que resultan en nodos que se añaden a #q# son coloreadas blancas, las otras
  aristas son coloreadas gris.}
  \figlabel{graph-bfs}
\end{figure}

El análisis del tiempo de ejecución de la rutina #bfs(g,i)# es relativamente
directo. El uso del array #seen# asegura no vértice será añadido a #q# más
de una vez. Añadir (y después remover) cada vértice de #q# toma tiempo constante
por vértice para un total de tiempo $O(#n#)$. Dado que cada vértices es
procesado por el bucle interior como máximo una vez, 
cada lista adyacente es procesad como máximo una vez, así que cada arista de 
$G$ es procesada como máximo una vez. Este procesamiento, el cual se
realiza en el bucle interior que toma tiempo constante por iteración, para
un total de tiempo $O(#m#)$. Por lo tanto, el algoritmo completo se ejecuta
en tiempo $O(#n#+#m#)$.

El teorema siguiente sintetiza el rendimiento del algoritmo #bfs(g,r)#.
\begin{thm}\thmlabel{bfs-graph}
    Cuando se da como entrada una estructura #Graph#, #g#, que se implementa
    usando una estructura de datos #AdjacencyLists#, el algoritmos #bfs(g,r)#
    se ejecuta en tiempo $O(#n#+#m#)$.
\end{thm}

Un recorrido de anchura tiene algunas propiedades muy especiales.
Un llamado a #bfs(g,r)# eventualmente encolará (y eventualmente descolará) cada
vértices #j# tal que hay una ruta dirigida desde #r# a #j#.  Además,
los vértices dentro de una distancia 0 de #r# (#r# mismo) entrarán #q# antes
que los vértices a una distancia 1, los cuales entrarán  #q# antes que los
vértices a una distancia 2, y así sucesivamente. Así que, el método #bfs(g,r)#
visita los vértices en orden ascendente de distancias de #r# y aquellos vértices
que no son accesibles desde #r# nunca se visitan.

Una aplicación particularmente útil del algoritmo de \emph{búsqueda en anchura}
es, por lo tanto, en la computación de las rutas más cortas. Para computar 
la ruta más corta desde #r# a cualquier otro vértice, usamos una variante de 
#bfs(g,r)# que usa un array auxiliar, #p#, de longitud #n#.  Cuando un vértice nuevo
#j# se añade a #q#, nosotros asignamos #p[j]=i#.  De esta manera, #p[j]# 
se convierte en el penúltimo nodo en una ruta más corta desde #r# a #j#. Repitiendo
esto, al tomar #p[p[j]#, #p[p[p[j]]]#, y así sucesivamente, podemos reconstruir 
(la inversión de) una ruta más corta desde #r# a #j#.

\subsection{Búsqueda en Profundidad (Depth-First Search)}

El algoritmo de \emph{búsqueda en profundidad}
\index{búsqueda!en profundidad (depth-first-search)}%
es similar al algoritmo estándar para recorrer árboles binarios; primero
completamente explora un sub-árbol antes de retorna al nodo actual
y después explora los otros sub-árboles. Otra manera de pensar 
sobre este algoritmos es al decir que es similar al algoritmo 
de búsqueda en profundidad excepto que usa una pila en lugar de
un cola.

Durante la ejecución del algoritmo de búsqueda en profundidad, cada vértice,
#i#, se el asigna un color, #c[i]#: #white# si nunca hemos visto el 
vértice anteriormente, #grey# si estamos visitando el vértice actualmente,
y #black# y si ya hemos visitado el vértice.  La forma más fácil de pensar
sobre búsqueda en profundidad es considerarlo como un algoritmo recursivo. 
El mismo comienza con visitar #r#. Cuando visita un vértice #i#, primero
marcamos a #i# como #grey#. Siguiente, escaneamos a la lista adyacente
de #i# y recursivamente visitamos cualquier vértice blanco que encontramos
en esta lista. Finalmente, terminamos con procesar a #i#, así que coloreamos
a #i# negro y retornamos.

\codeimport{ods/Algorithms.dfs(g,r).dfs(g,i,c)}

Un ejemplo de la ejecución de este algoritmo se muestra en la \figref{graph-dfs}.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/graph-dfs}
  \end{center}
  \caption[Búsqueda en profundidad]Un ejemplo de búsqueda en profundidad comenzando con el nodo 0.
  Los nodos son etiquetados en el orden en el cual son procesados. Las aristas que resultan
  en un llamada recursiva son coloreadas negras, las otras aristas son coloreadas
  gris.}
  \figlabel{graph-dfs}
\end{figure}

Aunque búsqueda en profundidad puede considerarse como un algoritmo recursivo,
recursión no es una de la mejor manera de implementarlo. De hecho, 
el código dado más arriba fallará para muchos de los grafos grandes al causar
un desbordamiento de pila (\emph{stack overflow}). Una implementación alternativa
es reemplazar la pila de recursión con una pila explícita, #s#. La implementación
siguiente hace exactamente eso:

\codeimport{ods/Algorithms.dfs2(g,r)} 

En el código anterior, cuando el vértice siguiente, #i#, es procesado, #i#
es coloreado #grey# y después reemplazado, en la pila, con sus vértice adyacentes.
Durante la iteración siguiente, uno de esto vértices será visitado.

Como ha de esperarse, los tiempos de ejecución de #dfs(g,r)# y #dfs2(g,r)# 
son los mismos que los de #bfs(g,r)#:
\begin{thm}\thmlabel{dfs-graph}
  Cuando se da como entrada una estructura #Graph#, #g#, que se implementa usando
  la estructura de datos #AdjacencyLists#, los algoritmos #dfs(g,r)# y #dfs2(g,r)#
  se ejecutan cada uno en tiempo $O(#n#+#m#)$.
\end{thm}

Como con el algoritmo de búsqueda en anchura, existe un árbol subyacente asociado
con cada ejecución de búsqueda en profundidad. Cuando un nodo
$#i#\neq #r#$ va desde #white# a #grey#, esto es porque #dfs(g,i,c)#
fue llamado recursivamente durante el procesamiento de algún nodo #i'#.
(En el caso del algoritmo #dfs2(g,r)#, #i# es uno de los nodos reemplazados por #i'#
en la pila.)  Si consideramos a #i'# como el padre de #i#, entonces obtenemos
un árbol arraigado en #r#. En la \figref{graph-dfs}, este árbol es una ruta del
vértice 0 al vértice 11.

Una propiedad importante del algoritmo de búsqueda en profundidad
es la siguiente: Supón que cuando un nodo #i# es coloreado #grey#, existe
una ruta desde #i# a algún otro nodo #j# que usa solamente vértices blancos.
Entonces #j# será coloreado primero #grey# después #black# antes que #i# 
sea coloreado #black#. (Esto puede demostrarse por contradicción, al
considerar cualquier ruta $P$ desde #i# a #j#.)

Una aplicación de esta propiedad es la detección de ciclos.
\index{detección!de ciclo}%
Refiérete a la \figref{dfs-cycle}. Considera algún ciclo, $C$, puede alcanzarse
desde #r#. Sea #i# el primer nodo de $C$ que está coloreada #grey#,
y sea #j# el nodo que precede a #i# en el ciclo $C$.  Entonces,
por la propiedad más arriba, #j# será coloreada #grey# y la arista #(j,i)#
será considerada por el algoritmo mientras que #i# es aún #grey#.  Por lo tanto,
el algoritmo puede concluir que existe una ruta, $P$, desde #i# a #j#
en el árbol de búsqueda en profundidad y la arista #(j,i)# existe. Por consiguiente,
$P$ es también un ciclo.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/dfs-cycle}
  \end{center}
  \caption[Detección de ciclo]{El algoritmo de búsqueda en profundidad puede usarse
  para detectar ciclos en $G$. El nodo #j# es coloreado #grey# mientras que #i#
  es aún #grey#. Esto implica que hay una ruta, $P$, desde #i# a #j# en el árbol de
  búsqueda en profundidad, y la arista #(j,i)# implica que $P$ es también un ciclo.}
  \figlabel{dfs-cycle}
\end{figure}

\section{Discusión y Ejercicios}

Los tiempos de ejecución de los algoritmos de búsqueda en profundidad
y búsqueda en anchura son un poco exagerados por los
Teoremas~\ref{thm:bfs-graph} y \ref{thm:dfs-graph}.
Define a $#n#_{#r#}$ como el número de vértices, #i#, de $G$, para los cuales
existe una ruta desde #r# a #i#.  Define a $#m#_#r#$ como el número de aristas
que tienen estos vértices como orígenes. Entonces el teorema siguiente es
una afirmación más precisa de los tiempos de ejecución de los algoritmos de 
búsqueda en profundidad y búsqueda en anchura. 
(Esta afirmación más refinada del tiempo de ejecución es útil en algunas de
las aplicaciones de estos algoritmos esbozadas en los ejercicios.)

\begin{thm}\thmlabel{graph-traversal}
    Dada como entrada una estructura #Graph#, #g#, que es implementada usando la
    estructura de datos #AdjacencyLists#, los algoritmos #bfs(g,r)#, #dfs(g,r)#
    y #dfs2(g,r)# se ejecutan en tiempo $O(#n#_{#r#}+#m#_{#r#})$.
\end{thm}

Búsqueda en anchura aparece haber sido descubierto independientemente por 
Moore \cite{m59} y Lee \cite{l61} en los contextos de exploración de laberintos 
y el enrutamiento de circuitos, respectivamente.

Las representaciones con listas adyacentes de grafos fueron presentadas por
Hopcroft y Tarjan \cite{ht73} como una alternativo a la representación
con matrices adyacentes, la cual era mucho más común en aquel entonces. 
Esta representación, al igual que búsqueda en profundidad, desempeño un papel
importante en el celebrado algoritmo de prueba de planaridad de Hopcroft-Tarjan
\index{grafo!prueba de planaridad}%
que puede determinar, en tiempo $O(#n#)$, si un grafo puede dibujarse, en el
plano, y de tal manera que no pares de aristas se cruzan
\cite{ht74}.

En los ejercicios siguientes, un grafo no dirigido es un grafo en el cual, 
por cada #i# y #j#, la arista $(#i#,#j#)$ está presente si y solo si 
la arista $(#j#,#i#)$ está presente.
\index{grafo!no dirigido}%

\begin{exc}
    Dibuja una representación de una lista adyacente y un representación
    de una matriz adyacente del grafo en la \figref{graph-example2}.
\end{exc}

\begin{figure}
  \centering{\includegraphics[scale=0.90909]{figs/graph-example2}}
  \caption{Un ejemplo de un grafo.}
  \figlabel{graph-example2}
\end{figure}

\begin{exc}
  \index{matriz!de incidencia}%
  La representación de la \emph{matriz de incidencia} de un grafo,
  $G$, es una matriz $#n#\times#m#$, $A$, donde
  \[
     A_{i,j} = \begin{cases}
        -1 & \text{si vértice $i$ es el origen de la arista $j$} \\
        +1 & \text{si vértice $i$ es el destino de la arista $j$} \\
        0 & \text{de lo contrario.}
     \end{cases}
  \]
  \begin{enumerate}
    \item Dibuja la representación de la matriz de incidencia del grafo en
      la \figref{graph-example2}.
    \item Diseña, analiza e implementa una representación de una
    matriz de incidencia de un grafo. Asegúrate de analizar el espacio, el
    costo  de #addEdge(i,j)#, #removeEdge(i,j)#, #hasEdge(i,j)#, #inEdges(i)#,
    y #outEdges(i)#.
  \end{enumerate}
\end{exc}

\begin{exc}
    Ilustra una ejecución de #bfs(G,0)# y #dfs(G,0)# en el grafo,
    $#G#$, en la \figref{graph-example2}.
\end{exc}

\begin{exc}
  \index{grafo!conexo}%
    Sea $G$ un grafo no dirigido. Decimos que $G$ es \emph{conexo} si,
    por cada par de vértices #i# y #j# en $G$, hay una ruta desde
    $#i#$ a $#j#$ (dado que $G$ no es dirigido, también hay una ruta desde #j#
    a #i#). Muestra cómo probar si $G$ es conectado en tiempo $O(#n#+#m#)$.
\end{exc}

\begin{exc}
  \index{grafo!componentes conexos}%
    Sea $G$ un grafo no dirigido. Una \emph{etiqueta de componentes conexos}
    de $G$ divide los vértices de $G$ en conjuntos máximos, cada uno de los
    cuales forma un sub-grafo conectado. Muestra cómo calcular una
    etiqueta de componentes conexos de $G$ en tiempo $O(#n#+#m#)$.
\end{exc}

\begin{exc}
  \index{bosque!de expansión}%
    Sea $G$ un grafo no dirigido. Un \emph{bosque de expansión} de $G$ es una
    colección de árboles, uno por componentes, cuyas aristas son aristas de $G$
    y cuyos vértices contienen todos los vértices de $G$.  Muestra cómo calcular
    un bosque de expansión de $G$ en tiempo $O(#n#+#m#)$.
\end{exc}

\begin{exc}
  \index{grafo!fuertement conexo}%
    Decimos que un grafo $G$ es \emph{fuertemente conexo} si, por cada
    par de vértices #i# y #j# en $G$, existe una ruta desde $#i#$ a $#j#$.
    Muestra cómo probar que $G$ es fuertemente conexo en tiempo $O(#n#+#m#)$.
\end{exc}

\begin{exc}
    Dado un grafo $G=(V,E)$ y algún vértice especial $#r#\in V$, muestra cómo
    calcular la longitud de la ruta más corta desde $#r#$ a $#i#$ por cada
    vértice $#i#\in V$.
\end{exc}

\begin{exc}
    Dado un ejemplo (simple) donde el código #dfs(g,r)# visita los nodos de un
    grafo en un orden que es diferente a aquel del código #dfs2(g,r)#.
    Escribe una versión de #dfs2(g,r)# siempre visita los nodos en exactamente
    el mismo orden que #dfs(g,r)#. (Pista: Comienza con trazar la ejecución
    de cada algoritmo en algún grafo donde #r# es el origen de más de 1 arista.)
\end{exc}

\begin{exc}
  \index{vértice!universal}%
  \index{celebridad|ver{vértice universal}}%
    Un \emph{vértice universal} en un grafo $G$ es un vértice que es el destino 
    de $#n#-1$ aristas y el origen de no aristas. \footnote{Un universal vértice,
    #v#, también es llamado un \emph{celebridad}: Cada persona en el salón
    reconoce a #v#, pero #v# no reconoce a nadie en el salón.}
    Diseña e implementa un algoritmo que prueba si un grafo $G$, representado
    como una estructura #AdjacencyMatrix#, tiene un vértice universal. 
    Tu algoritmo debería ejecutarse en tiempo $O(#n#)$.
\end{exc}