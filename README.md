## Spanish Open Data Structures (ODS)

I'm trying to translate Pat Morin's Open Data Structures to Spanish
while I'm learning about these data structures.

The original project can be found at https://github.com/patmorin/ods. You
might find this repo is a lot slimmer than the original one and this is because
I removed the C++ and Java directories which were preventing me to compile
the latex sources into the (pseudocode) pdf. Please check out
[Morin's repo](https://github.com/patmorin/ods) for additional information.


|Chapter | Translated | Revised |
|--|--|--|
|1. Introduction | ✓ |  |
|2. Array-Based Lists | ✓ |  |
|3. Linked Lists | ✓ |  |
|4. Skiplists | ✓ |  |
|5. Hash Tables | ✓ |  |
|6. Binary Trees | ✓ |  |
|7. Random Binary Search Trees | ✓ |  |
|8. Scapegoat Trees | ✓ |  |
|9. Red-Black Trees | ✓ |  |
|10. Heaps | ✓ |  |
|11. Sorting Algorithms | ✓ |  |
|12. Graphs| ✓ |  |
|13. Data Structures for Integers| ✓ |  |
|14. External Memory Search| ✓  |  |

---

## Directory structure

- `latex/` contains the latex sources
- `python/ods` contains the python sources

## Creating the books

To compile the latex sources into the book (ods-python.pdf):

```
mkdir ~/texmf/tex/latex/ods/
cp ods-colors.sty ~/texmf/tex/latex/ods/
cd latex ; make
```

This will require a decent installation of `pdflatex`, `perl`, `ipe`,
`inkscape`, `gnuplot`, and `pdftk`.

If you have problems with tikz figures, consult the solution here:
https://tex.stackexchange.com/q/124850

If `ipetoipe` generates errors about `ods-colors.sty`, then try this:

```
mkdir -p ~/texmf/tex/latex/ods/
ln -s $PWD/latex/ods-colors.sty ~/texmf/tex/latex/ods/
texhash
```

## What's in here:

- `python`         - The Python code used to generate the pseudocode version
- `python/tests`   - Unit tests for the Python code
- `latex`          - The book's latex source code and scripts
- `latex/chapters` - The book chapters' latex source code
- `latex/figs`     - The book's ipe figures
- `latex/images`   - Images used in the book

## How it works

The Makefile and Perl script in `./latex` do the following:

1. Convert ipe figures in `./latex/figs` into pdf
2. Convert svg figures in `./latex/images` into pdf
3. Scan the latex sources and generate `-java.tex` and `-cpp.tex` files
   that include source code from `./java` and `./cpp` directories
4. Run `pdflatex` and `bibtex` to generate the file `ods-java.pdf` and
   `ods-cpp.pdf`

